Meteor.methods({
	track: function (data) {
		check(data, Match.ObjectIncluding({
			version: String,
			userId: String,
			time: Number,
			type: String,
		}));

		TrackingCollection.insert(data);
	},
	contact: function (data) {
		// Let other method calls from the same client start running,
    	// without waiting for the email sending to complete.
    	this.unblock();

		var from = "info@sinceidea.de";
		var to = "idle.hero@sinceidea.de";

    	var subject = 'Idle-Hero Extra Domi Contact Attempt';
		var text = '';

		for (var i in data) {
			text += i + ': ' + data[i] + '\n';
		}

		try {
			Email.send({
				to: to,
				from: from,
				subject: subject,
				text: text
			});
			data.sentSuccess = true;
		} catch (e) {
			data.error = e;
			data.sentSuccess = false;
		}
		
		ContactsCollection.insert(data);
		console.log(data);
	}
});