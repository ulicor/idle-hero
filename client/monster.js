

Template.monster.player = function () {
	return SessionAmplify.get('playerSettings');
}

Template.monster.playerImg = function (gender) {
	if (gender==='m') {
		return '/heroes/hero.png';
	}
	return '/heroes/heroin.png';
}

Template.monster.damage = function () {
	var effects = SessionAmplify.get('hero.effects');
	return effects.att;
}

Template.monster.events({
	'click #monster, touchstart #monster, MozTouchDown #monster': function (e) {
		e.preventDefault();

		SessionAmplify.set('clickCounter', SessionAmplify.get('clickCounter')+1)

		doBattle(true);
	},
})