SessionAmplify.setDefault('quest', null);
SessionAmplify.setDefault('quest.success', false);

var oneMinute = 1000 * 60;

questDurations = [
	//5000,
	oneMinute * 5,
	oneMinute * 15,
	oneMinute * 60,
	oneMinute * 60 * 6,
	oneMinute * 60 * 24,
];

SessionAmplify.setDefault('lastAcceptedQuest', questDurations[0]);
Session.set('selectedQuest', SessionAmplify.get('lastAcceptedQuest'));

var acceptQuest = function (duration) {
	var language = SessionAmplify.get('language')
	var quest = {
		duration : duration,
		acceptTime : +new Date,
		name : Random.choice(i18n[language].questnames),
	}

	track({
		type: 'acceptQuest',
		duration : quest.duration,
	})

	SessionAmplify.set('quest', quest);
	SessionAmplify.set('quest.progress', 0);
	SessionAmplify.set('quest.remaining', duration);
	SessionAmplify.set('quest.success', false);
	SessionAmplify.set('lastAcceptedQuest', duration);
}

var cancelQuest = function () {
	track({
		type: 'cancelQuest',
	});

	SessionAmplify.set('quest', null);
}


var receiveQuestReward = function () {
	var quest = SessionAmplify.get('quest');
	var level = SessionAmplify.get('hero.level');
	var exp = SessionAmplify.get('hero.exp');
	var gold = SessionAmplify.get('hero.gold');

	var goldGain = parseInt(exponetialGain(
		quest.duration, 
		config.questGoldBaseMultiplier, 
		config.questGoldExponent, 
		config.questGoldMultiplier
	) * level / 50);
	var expGain = exponetialGain(
		quest.duration, 
		config.questExpBaseMultiplier, 
		config.questExpExponent, 
		config.questExpMultiplier
	);
	var levelGain = 0;

	gold += goldGain;
	exp += expGain;
	if (exp >= 100) {
		levelGain = Math.floor(exp/100);
		level += levelGain;
		exp %= 100;
		SessionAmplify.set('hero.level', level);
		showLevelUpMessage();
	}
	SessionAmplify.set('hero.exp', exp);
	SessionAmplify.set('hero.gold', gold);
	SessionAmplify.set('quest', null);
	SessionAmplify.set('quest.success', false);

	track({
		type: 'receiveQuestReward',
		goldGain : goldGain,
		expGain : expGain,
		levelGain : levelGain,
		level: level,
	})
}

Template.quest.questDurations = function () {
	return questDurations;
}

Template.quest.questRemaining = function () {
	return SessionAmplify.get('quest.remaining');
}

Template.quest.questProgress = function () {
	var remaining = SessionAmplify.get('quest.remaining');
	var quest = SessionAmplify.get('quest');
	if (!quest) return 0;
	var progress = (1 - (remaining / quest.duration)) * 100;
	return progress;
}

Template.quest.hasQuest = function () {
	return !SessionAmplify.equals('quest', null);
}

Template.quest.quest = function () {
	return SessionAmplify.get('quest');
}

Template.quest.questGain = function () {
	var duration = Session.get('selectedQuest');

	var quest = SessionAmplify.get('quest');
	if (quest) {
		duration = quest.duration;
	}
	var level = SessionAmplify.get('hero.level');
	
	var gain = {
		gold : parseInt(exponetialGain(
			duration, 
			config.questGoldBaseMultiplier, 
			config.questGoldExponent, 
			config.questGoldMultiplier
		) * level / 50),
		exp : exponetialGain(
			duration, 
			config.questExpBaseMultiplier, 
			config.questExpExponent, 
			config.questExpMultiplier
		),
	}
	return gain;
}

Template.quest.lastAcceptedQuestSelected = function (value) {
	return SessionAmplify.equals('lastAcceptedQuest', value) ? {selected: true} : {};
}

Template.quest.events({
	'click #acceptQuest' : function (e, t) {
		acceptQuest(parseInt($(t.find('#questDurationSelect')).find(':selected').val()));
	},
	'change #questDurationSelect' : function (e, t) {
		Session.set('selectedQuest', e.target.options[e.target.selectedIndex].value);
	},
	'click #cancelQuest' : function (e, t) {
		cancelQuest();
	}, 
	'click #receiveQuestReward' : function (e, t) {
		receiveQuestReward();
	}, 
});