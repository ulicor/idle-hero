submitContact = function (data) {
	data.version = config.version;
	data.userId = SessionAmplify.get('userId');
	data.time = +new Date;
	var player = SessionAmplify.get('playerSettings');
	data.playername = player.name;
	data.playeremail = player.email;

//	console.log('submitContact', data);
	Meteor.call('contact', data, function (error, result) {
		if (error) {
			console.log(error, data);
		}
	});
}

var setDefaultContact = function () {

			contactNameInput.val('');
			contactContactInput.val('');
			contactTitleInput.val('');
			contactTextInput.val('');
}

Template.contact.rendered = function () {
	Session.set('contact.hasError', false);
	Session.set('contact.success', []); 
}


Template.contact.events({
	'click #contact-submit' : function (e, t) {
		var data = {};
		var errors = [];
		var success = [];
		var hasError = false;
		// form fields
		Session.set('contact.success', []); 
		contactNameInput = $(t.find('#contact-name'));
		contactContactInput = $(t.find('#contact-contact'));
		contactTitleInput = $(t.find('#contact-title'));
		contactTextInput = $(t.find('#contact-text'));

		// check formular
		data.contactName = contactNameInput.val();
		data.contactContact = contactContactInput.val();
		data.contactTitle = contactTitleInput.val();
		data.contactText = contactTextInput.val();

		if (data.contactText === "") {
			hasError = true;
			errors.push("error_empty"); 
		}

		Session.set('contact.hasError', hasError);
		Session.set('contact.errors', errors); 

		//console.log(data);

		// submit data
		if (!hasError) {
			submitContact(data);

			setDefaultContact();

			success.push("success_send");
		}
		Session.set('contact.success', success); 
	}
})