Deps.autorun(function () {
	if (SessionAmplify.equals('page', 'tavern')) {
		Session.set('questCompleted', false);
	}
});

var tavernTick = function () {
	var currentTime = +new Date;
	var lastTavernTick = SessionAmplify.get('lastTavernTick');
	var deltaTime = currentTime - lastTavernTick;

	// quest
	var quest = SessionAmplify.get('quest');
	var success = SessionAmplify.get('quest.success');

	if (quest && !success) {

		var remaining = SessionAmplify.get('quest.remaining');
		remaining -= deltaTime;
		if (remaining <= 0) {
			remaining = 0;
			SessionAmplify.set('quest.success', true);
		}
		SessionAmplify.set('quest.remaining', remaining);

	}

	// potion
	var potion = SessionAmplify.get('potion');
	var ended = SessionAmplify.get('potion.ended');

	if (potion && !ended) {
		var remaining = SessionAmplify.get('potion.remaining');
		remaining -= deltaTime;
		if (remaining <= 0) {
			remaining = 0;
			SessionAmplify.set('potion.ended', true);
		}
		SessionAmplify.set('potion.remaining', remaining);
	}


	SessionAmplify.set('lastTavernTick', currentTime);
	Meteor.setTimeout(tavernTick, 1000);
}
tavernTick();

var oneMinute = 1000 * 60;
var oneMio = 1000000;

effectPricesMultiplier = {
	hp : 1,
	att : 5,
	def : 5,
}

var getUpdatedEffectPriceIncreaseTracker = function (effect, number) {
	var effectPriceIncreaseTracker = SessionAmplify.get('effectPriceIncreaseTracker.'+effect);

	if (typeof effectPriceIncreaseTracker === "undefined" || effectPriceIncreaseTracker.date < new Date - (1000*60*60*20)) {
		//first set then get to set the dependency
		SessionAmplify.set('effectPriceIncreaseTracker.'+effect, {
			number: 0,
			date: +new Date,
		});
		effectPriceIncreaseTracker = SessionAmplify.get('effectPriceIncreaseTracker.'+effect);
	}

	effectPriceIncreaseTracker.number += number;

	return effectPriceIncreaseTracker;
}

var calcEffectPrice = function (effect, number) {
	if (number < 1 || isNaN(number)) number = 1;

	var effectPriceIncreaseTracker = getUpdatedEffectPriceIncreaseTracker(effect, 0);
	
	var price = effectPricesMultiplier[effect] * (Math.pow(2, effectPriceIncreaseTracker.number+number+1) - Math.pow(2, effectPriceIncreaseTracker.number+1));

	return price;
}

availablePotions = [
	{
		effect : 'att',
		bonus : +10,
		cost : 3 * oneMio,
		duration : oneMinute,
	},
	{
		effect : 'def',
		bonus : +10,
		cost : 3 * oneMio,
		duration : oneMinute,
	},
	{
		effect : 'hp',
		bonus : +10,
		cost : 3 * oneMio,
		duration : oneMinute,
	},
];

SessionAmplify.setDefault('lastBoughtEffect', 'hp');
Session.setDefault('selectedEffect', SessionAmplify.get('lastBoughtEffect'));
Session.setDefault('selectedNumber', 1);

var buyEffect = function (effect) {

	var heroGold = SessionAmplify.get('hero.gold');
	var number = parseInt(Session.get('selectedNumber'));
	if (number < 1 || isNaN(number)) number = 1;
	var price = calcEffectPrice(effect, number);
	if (price > heroGold) {
		return alert('cant complete buyEffect');
	}

	var baseEffects = SessionAmplify.get('hero.baseEffects');
	if (typeof baseEffects[effect] === "undefined") {
		return alert('undefined baseEffect '+effect);
	}

	SessionAmplify.set('hero.gold', heroGold - price);

	baseEffects[effect] = parseInt(baseEffects[effect]) + number;

	track({
		type : 'buyEffect',
		effect : effect,
		number : number,
		baseEffects : baseEffects,
	})

	var effectPriceIncreaseTracker = getUpdatedEffectPriceIncreaseTracker(effect, number);
	SessionAmplify.set('effectPriceIncreaseTracker.'+effect, effectPriceIncreaseTracker);

	SessionAmplify.set('hero.baseEffects', baseEffects);
	SessionAmplify.set('lastBoughtEffect', effect);
}

Template.potions.effectPricesMultiplier = function () {
	return effectPricesMultiplier;
}

Template.potions.effectBuyDisabled = function () {
	var effect = Session.get('selectedEffect');
	var number = Session.get('selectedNumber');
	if (number < 1 || isNaN(number)) number = 1;
	var gold = SessionAmplify.get('hero.gold');
	return gold < calcEffectPrice(effect, number) ? {disabled:true} : {};
}

Template.potions.effectSelected = function (effect) {
	return SessionAmplify.equals('lastBoughtEffect', effect) ? {selected : true} : {};
}

Template.potions.selectedPrice = function () {
	var effect = Session.get('selectedEffect');
	var number = Session.get('selectedNumber');
	if (number < 1 || isNaN(number)) number = 1;
	return calcEffectPrice(effect, number);
}

Template.potions.events({
	'click #buyEffect' : function (e, t) {
		buyEffect($(t.find('#buyEffectSelect')).find(':selected').val());
	}, 
	'change #buyEffectSelect' : function (e, t) {
		Session.set('selectedEffect', e.target.options[e.target.selectedIndex].value);
	},
	'keyup #buyEffectNumber, change #buyEffectNumber' : function (e, t) {
		var number = parseInt(e.target.value);
		if (number < 1 || isNaN(number)) {
			number = 1;
			e.target.value = '1';
		}
		Session.set('selectedNumber', parseInt(e.target.value));
	}
});