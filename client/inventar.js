Session.setDefault("inventarFilter", equipTypes[0]);

Template.inventar.inventarList = function () {
	var filter = Session.get('inventarFilter');
	var inventarList = [];

	/*if (typeof filter === "undefined") {
		inventarList=SessionAmplify.get('inventar.' + equipTypes[0])
			.concat(SessionAmplify.get('inventar.' + equipTypes[1]))
			.concat(SessionAmplify.get('inventar.' + equipTypes[2]));
	} else //*/
	{
		inventarList = SessionAmplify.get('inventar.' + filter);
	}
	//console.log (inventarList);
	return inventarList;
}

Template.inventar.equipped = function () {
	var filter = Session.get('inventarFilter');
	
	var equipped = SessionAmplify.get('equipment.' + filter);
	
	return equipped;
}

Template.inventar.events ({
	'click .inventarFilterChoice' : function (e, t) {
		Session.set('inventarFilter', $(e.target).attr('data-type'));
	},
	'click .equipItem': function () {
		equipFromInventory(this)
	},
	'click .sellItem': function () {
		sellItem(this);
	},

	'mouseenter .shopItem' : function (e,t) {
		var item = e.target;

		if ($(item).hasClass('inventarEquipped')) {
			return;
		}
		
		var classes = item.getAttribute('class').split(' ');
		var type = classes[classes.indexOf('shopItem')+1];
		$(item).addClass('comparedItem');

		Session.set('shop.comparetype', type);
		//console.log($(item).offset());

		Meteor.setTimeout(function () {
			var pos = $(item).offset();
			$('#compareItem').css({
				top: (pos.top - 20) + 'px',
				left: (pos.left + 320) + 'px',
			}).show();
		}, 100);
	},
	'mouseleave .shopItem' : function (e,t) {
		//console.log('leave');
		var item = e.target;
		$(item).removeClass('comparedItem');
		Session.set('shop.comparetype', false);
	},
	'scroll #inventarList' : function (e,t) {
		var comparetyp = Session.get('shop.comparetype');

		if (!(typeof comparetyp === 'undefined' || !comparetyp)) {
			var item = $('.comparedItem');
			var pos = item.offset();
			$('#compareItem').css({
				top: (pos.top - 20) + 'px',
				left: (pos.left + 320) + 'px',
			})
		}
	}
});