var lastAnimate = null;
Deps.autorun(function () {
	var level = SessionAmplify.get('hero.level');
	var exp = SessionAmplify.get('hero.exp');
	if (exp > 100) exp = 100;

	var bar = $('.expBar');
	if (bar.length === 0 || typeof exp === "undefined") {
		return;
	}

	var currentTime = +new Date;
	var animDuration = 500;

	if (lastAnimate) {
		animDuration = Math.min(animDuration, currentTime-lastAnimate.time);

		//sometimes the queue bilds up, i didnt find a better way to solve this problem then this
		//basicly substract the queue size from the aniamtion duration
		animDuration = Math.max(0, animDuration - ((bar.queue("fx")||[]).length) - 10);
	}

	if (lastAnimate && lastAnimate.level < level) {
		if (exp == 0) {
			bar
			.animate({
				width: "100%"
			}, animDuration)
			.animate({
				width: "0%"
			}, 0)
		} else {
			bar
			.animate({
				width: "100%"
			}, animDuration/2)
			.animate({
				width: "0%"
			}, 0)
			.animate({
				width: exp + "%"
			}, animDuration/2)
		}
	} else {
		bar
		.animate({
			width: exp + "%"
		}, animDuration)
	}

	lastAnimate = {
		level: level,
		exp: exp,
		time: currentTime
	}
});