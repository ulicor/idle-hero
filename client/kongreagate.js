var lastClickc = [];
var throttledClickc = _.throttle(function (clicks) {
	lastClickc.push(clicks);
	if (lastClickc.length > 20) {
		var dob = true;
		for (var i = 1; i < lastClickc.length; i++) {
			if (lastClickc[i] - lastClickc[i-1] < 20) {
				dob = false;
			}
		}
		if (dob) {
			SessionAmplify.set('hb', true);
			SessionAmplify.set('hb2', true);
			track({
				type: 'hb',
			});
		} else {
			SessionAmplify.set('hb', false);
		}
		lastClickc.splice(0,1);
	}
}, 1000);
Deps.autorun(function () {
	throttledClickc(SessionAmplify.get('clickCounter'));
});

var startHighscoreSubmits = function () {
	var updateLevel = _.throttle(function (value) {
		kongregate.stats.submit("Level", value);
	}, 5000);
	Deps.autorun(function () {
		if (SessionAmplify.get('hb') || SessionAmplify.get('hb2')) return;
		updateLevel(SessionAmplify.get('hero.level'));
	});

	var updateClicks = _.throttle(function (value) {
		kongregate.stats.submit("Clicks", value);
	}, 5000);
	Deps.autorun(function () {
		if (SessionAmplify.get('hb') || SessionAmplify.get('hb2')) return;
		updateClicks(SessionAmplify.get('clickCounter'));
	});


	var updateEhp = _.throttle(function (value) {
		kongregate.stats.submit("ehp", value);
	}, 5000);
	Deps.autorun(function () {
		if (SessionAmplify.get('hb') || SessionAmplify.get('hb2')) return;
		updateEhp(Math.round(Session.get('hero.ehp')));
	});

	var updateDps = _.throttle(function (value) {
		kongregate.stats.submit("dps", value);
	}, 5000);
	Deps.autorun(function () {
		if (SessionAmplify.get('hb') || SessionAmplify.get('hb2')) return;
		updateDps(Math.round(Session.get('hero.damage')));
	});

	var updateMonstersKilled = _.throttle(function (value) {
		kongregate.stats.submit("monstersKilled", value);
	}, 5000);
	Deps.autorun(function () {
		if (SessionAmplify.get('hb') || SessionAmplify.get('hb2')) return;
		updateMonstersKilled(SessionAmplify.get('monstersKilled'));
	});

	var updateDungeonsCompleted = _.throttle(function (value) {
		kongregate.stats.submit("dungeonsCompleted", value);
	}, 5000);
	Deps.autorun(function () {
		if (SessionAmplify.get('hb') || SessionAmplify.get('hb2')) return;
		updateDungeonsCompleted(SessionAmplify.get('dungeonsCompleted'));
	});
}

var initKongregate = function () {
	kongregateAPI.loadAPI(function () {
		kongregate = kongregateAPI.getAPI();
		//http://developers.kongregate.com/docs/kongregate-apis/handling-guests
		if(!kongregate.services.isGuest()){
			var player = SessionAmplify.get('playerSettings');
			if (typeof player.nameChoosen === "undefined" || !player.nameChoosen) {
				var kongName = kongregate.services.getUsername();
				if (player.name != kongName) {
					player.name = kongName;
					SessionAmplify.set('playerSettings', player);
				}
			}
			//kongregate.services.getGameAuthToken()
			startHighscoreSubmits();
		} else {
			//kongregate.services.showSignInBox()
		}
	})
}

var initKongregateIntervall = Meteor.setInterval (function () {
	if (typeof kongregateAPI !== "undefined") {
		Meteor.clearInterval(initKongregateIntervall);
		initKongregate();
	}
}, 50);