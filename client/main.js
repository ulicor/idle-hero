SessionAmplify.setDefault('page', 'combat');

Template.main.page = function () {
	return SessionAmplify.get('page');
}

Template.main.pageEquals = function (compareTo) {
	return SessionAmplify.equals('page', compareTo);
}

track({
	type: 'gameStart',
})
