
Template.sidebar.languageIsSelected = function (compareTo) {
	return SessionAmplify.equals('language', compareTo) ? {selected:true} : {};
}

Template.sidebar.events({
	'change #languageSelect': function (e, template) {
		var oldLanguage = SessionAmplify.get('language');
		var newLanguage = $(e.currentTarget).find(':selected').val();
		if (oldLanguage != newLanguage) {
			track({
				type: 'changedLanguage',
				oldLanguage: oldLanguage,
				newLanguage: newLanguage,
			});
			SessionAmplify.set('language', newLanguage);
		}
	}
})