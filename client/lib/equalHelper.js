Handlebars.registerHelper("SessionEquals", function(sessionKey, compare) {
	return Session.equals(sessionKey, compare);
});

Handlebars.registerHelper("SessionAmplifyEquals", function(sessionKey, compare) {
	return SessionAmplify.equals(sessionKey, compare);
});

