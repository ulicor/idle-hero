format = function (value, format) {
	if (format) {
		if (format === 'number') {
			return formatNumber(value);
		}
		if (format === 'colornumber') {
			return formatColorNumber(value);
		}
		if (format === 'ms') {
			return formatMs(value);
		}
		if (format === 'exp') {
			return formatExp(value);
		}
		if (format === 'percent') {
			return formatPercent(value);
		}
		if (format === 'percentNormalized') {
			return formatPercent(value*100);
		}
	}

	return value;
}

var endingChars = [
	1000000000000000, //billiarden
	1000000000000, //billion
	1000000000, //milliarden
	1000000, //million
	1000, //tausend
];
formatNumber = function (value) {
	if (value === Infinity || value === -Infinity) {
		return value;
	}

	var language = SessionAmplify.get('language');

	value = Math.round(value);
	var valueAbs = Math.abs(value);

    var endingChar = '';
    value *= 100;
	for (var i in endingChars) {
	    if (valueAbs > endingChars[i]*10) {
	    	value /= endingChars[i];
	    	endingChar = i18n[language].format.endingChars[endingChars[i]];
	    	break;
	    }
	}
	value = Math.round(value);
	value /= 100;

    //add thousand delimeter
    value = value.toString()
    	.replace('.', i18n[language].format.digital) //replace the . with the lcoalized decimal divider
    	.replace(/\B(?=(\d{3})+(?!\d))/g, i18n[language].format.thausend) //add a thousand diviser atfer each 3 digits

    value += endingChar;

	return value;
}

formatColorNumber = function (value) {
	var color = '';
	if (value > 0) {
		color = 'pos';
	} else if (value < 0) {
		color = 'neg';
	} else {
		color = 'eq';
	}

	var number = formatNumber(value);

	return '<span class="numbercolor ' + color + '">' + number + '</span>';

}

formatMs = function (value) {
	var language = SessionAmplify.get('language');

	var result = "";

	var seconds = Math.round(value/1000);
	var minutes = Math.floor(seconds/60);
	seconds %= 60;
	var hours = Math.floor(minutes/60);
	minutes %= 60;
	var days = Math.floor(hours/24);
	hours %= 24;
	var weeks = Math.floor(days/7);
	days %= 7;

	if (weeks > 0) {
		result += weeks + " " + i18n[language].format.weeks + " ";
	}
	if (days > 0) {
		result += days + " " + i18n[language].format.days + " ";
	}
	if (hours > 0) {
		result += hours + " " + i18n[language].format.hours + " ";
	}
	if (minutes > 0) {
		result += minutes + " " + i18n[language].format.minutes + " ";
	}
	if (seconds > 0) {
		result += seconds + " " + i18n[language].format.seconds + " ";
	}

	return result;
}

formatExp = function (value) {
	var language = SessionAmplify.get('language');
	var level = parseInt(value/100);
	var exp = parseInt(value % 100);
	if (level > 0)  {
		return placeholderString(
			i18n[language].format.levelAndExp, 
			{
				level: level,
				exp : exp,
			}
		);
	}
	return placeholderString(
		i18n[language].format.exp, 
		{
			exp : exp,
		}
	);
}

formatPercent = function (value) {
	value = Math.round(value*10)/10;
	return value+"%";
}

Handlebars.registerHelper("format", function (value, options) {
  value = format(value, options.hash.format);
  
  return value;
});
