//http://stackoverflow.com/questions/11705825/persistent-sessions-in-meteor
SessionAmplify = _.extend({}, Session, {
  keys: _.object(_.map(amplify.store(), function(value, key) {
    return [key, JSON.stringify(value)]
  })),
  set: function (key, value) {
    Session.set.apply(this, arguments);
    amplify.store(key, value);
  },
});

Handlebars.registerHelper("SessionAmplify", function (key, options) {
  var value = SessionAmplify.get(key);

  value = format(value, options.hash.format);
  
  return value;
});

Handlebars.registerHelper("Session", function (key, options) {
  var value = Session.get(key);

  value = format(value, options.hash.format);

	return value;
});

//local storage variables
SessionAmplify.setDefault('userId', Random.hexString(24));
SessionAmplify.setDefault('gameStart', +new Date);

SessionAmplify.setDefault('hero.level', 1);
SessionAmplify.setDefault('hero.exp', 0);
SessionAmplify.setDefault('hero.effects', {});
SessionAmplify.setDefault('hero.gold', 100);

SessionAmplify.setDefault('clickCounter', 0);
SessionAmplify.setDefault('monstersKilled', 0);
SessionAmplify.setDefault('dungeonsCompleted', 0);

SessionAmplify.setDefault('lastAutoBattleTick', 0);

SessionAmplify.setDefault('equipment.weapon', null);
SessionAmplify.setDefault('equipment.armour', null);
SessionAmplify.setDefault('equipment.accessory', null);

SessionAmplify.setDefault('inventar.weapon', []);
SessionAmplify.setDefault('inventar.armour', []);
SessionAmplify.setDefault('inventar.accessory', []);

SessionAmplify.setDefault('selectedMonsterLevel', 1);

SessionAmplify.setDefault('nextStockUpdate', 0);
SessionAmplify.setDefault('lastStockUpdate', 0);

if (typeof SessionAmplify.get('language') === "undefined") {
  SessionAmplify.setDefault('language', 'en_GB');
}

//temp variables
Session.setDefault('hero.hpMax', 50);
Session.setDefault('hero.hpCurrent', 5);
Session.setDefault('hero.damage', 0);
Session.setDefault('hero.ehp', 0);
Session.setDefault('hero.aspd', 1);

Session.setDefault('shopItems', []);

Session.setDefault('welcomeBack', false);

Deps.autorun(function () {
  var level = SessionAmplify.get('hero.level');
  var effects = SessionAmplify.get('hero.effects');

  //this might happen on first load!
  if (typeof effects.hp === "undefined") {
    effects.hp = 0;
  }

  Session.set('hero.hpMax', level * 16 + effects.hp + 80);
});

resetSession = function () {
  track({
    type: 'resetSession',
  })

  localStorage.clear();
  location.reload();
}

var updateItem = function (item, level) {
  for (var i in item.effects) {
    if (i === 'crit') {
      item.effects[i] = item.effects[i]/level*1.5;
    }
    if (i === 'critDmg') {
      item.effects[i] = item.effects[i]/level*5;
    }
    if (i === 'aspd') {
      item.effects[i] = item.effects[i]/level/0.3;
    }
    if (i === 'loot') {
      item.effects[i] = 100;
    }
  }

  return item;
}
var updateItems = function () {
  var level = SessionAmplify.get('hero.level')/100;
  var equipTypes = ['weapon', 'armour', 'accessory'];
  _.each(equipTypes, function (element, index, list) {
    var item = SessionAmplify.get('equipment.' + element);
    var items = SessionAmplify.get('inventar.' + element);
    if (item) {
      item = updateItem(item, level);
      console.log(item.effects, item);
    }
    for (var i in items) {
      items[i] = updateItem(items[i], level);
      console.log(items[i].effects, items[i]);
    }
    SessionAmplify.set('equipment.' + element, item)
    SessionAmplify.set('inventar.' + element, items)
  });
}

var splittedHref = location.href.split('?i=');
if (splittedHref.length === 2) {
  var oldSave = JSON.parse(decodeURI(splittedHref[1]));
  for (i in oldSave) {
    SessionAmplify.set(i, oldSave[i]);
  }
  history.pushState({}, '', '/');
}

////////////////////////////////////////
//////////version migartion/////////////
////////////////////////////////////////
var version = SessionAmplify.get('version');
if (typeof version === "undefined") {
  //if the session version is udnefiend, we are either in a very old version or just started
  //detect which of those cases depending on the hero level
  //if we are above level 1 we prob did play before so reset the game
  if (SessionAmplify.get('hero.level') > 1) {
    resetSession();
  }
} else {
  switch(version) {
    case '0.0.0':
    case '0.0.1':
    case '0.0.2':
    case '0.0.3':
    case '0.0.4':
    case '0.0.5':
    case '0.0.6':
    case '0.1.0':
    case '0.1.1':
      updateItems();
    case '0.1.2':
    case '0.1.5':
      if (SessionAmplify.equals('page', 'monster')) {
        SessionAmplify.set('page', 'combat');
      }
    case '0.1.7':
      break;
  }
}
SessionAmplify.set('version', config.version);