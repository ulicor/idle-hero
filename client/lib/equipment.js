baseEffectsDefault = {
	att: 10,
	crit: 0,
	critDmg: 0,
	accuracy: 0,

	aspd: 0,
	
	def: 0,
	hp: 0,
	evasion: 0,

	loot: 0,
};

SessionAmplify.setDefault('hero.baseEffects', baseEffectsDefault);

equipTypes = ['weapon', 'armour', 'accessory'];

itemGuaranteedEffects = {
	weapon: 'att',
	armour: 'def',
	accessory: 'hp',
}

possibleEffects = {
	weapon: ['att', 'crit', 'critDmg', 'aspd', 'accuracy'],
	armour: ['def', 'hp', 'evasion'],
	accessory: ['crit', 'critDmg', 'aspd', 'hp', 'accuracy', 'loot', 'evasion']
}

buyItem = function (item) {
	var gold = SessionAmplify.get('hero.gold');
	gold -= item.cost

	if (gold >= 0) {
		SessionAmplify.set('hero.gold', gold);
		var currentItem = SessionAmplify.get('equipment.' + item.type);
		if (currentItem) {
			storeItem(currentItem);
		}
		equipItem(item);
	}
	track({
		type: 'buyItem',
		item : item,
	});
}

equipFromInventory = function (item) {
	var currentItem = SessionAmplify.get('equipment.' + item.type);
	if (currentItem) {
		//console.log(currentItem);
		storeItem(currentItem);
	}
	equipItem(item);
	removeItem(item);

	track({
		type: 'equipFromInventory',
		item : item,
	});
}

sellItem = function (item) {
	var gold = SessionAmplify.get('hero.gold');
	SessionAmplify.set('hero.gold', gold + item.cost);

	removeItem(item);

	track({
		type: 'sellItem',
		item : item,
	});
}

var storeItem = function (item) { // stores the given item to inventar
	
	var inventarArray = SessionAmplify.get('inventar.' + item.type);
	
	var newInventarArray = [];
	for (var i in inventarArray) {
		newInventarArray.push(inventarArray[i]);
	}
	newInventarArray.push(item);
	//console.log("inventarArray", newInventarArray);
	SessionAmplify.set('inventar.' + item.type, newInventarArray);

}

var removeItem = function (item) { // removes the given item from inventar
	var inventarArray = SessionAmplify.get('inventar.' + item.type);
	var newInventarArray = [];
	for (var i in inventarArray) {
		//console.log(inventarArray[i].toString());
		if (inventarArray[i].nameSeed !== item.nameSeed) {
			newInventarArray.push(inventarArray[i]);
		}
	}
	SessionAmplify.set('inventar.' + item.type, newInventarArray);
}

var equipItem = function (item) {
	SessionAmplify.set('equipment.' + item.type, item);
}

Deps.autorun(function () {
	var level = SessionAmplify.get('hero.level');
	var effects = SessionAmplify.get('hero.effects');
	var monstereffects = Session.get('monster.effects');

	var selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel');

	//calc hit/miss
	var hitChance = calcHitChanceFromEffects(
		effects, 
		level, 
		monstereffects, 
		selectedMonsterLevel
	);
	var monsterHitChance = calcHitChanceFromEffects(
		monstereffects, 
		selectedMonsterLevel, 
		effects, 
		level
	);

	Session.set('hitChance', hitChance);
	Session.set('monsterHitChance', monsterHitChance);
});

Deps.autorun(function () {
	var effects = SessionAmplify.get('hero.baseEffects');

	_.each(equipTypes, function (element, index, list) {
		var item = SessionAmplify.get('equipment.' + element)
		if (item) {
			for (var i in item.effects) {
				effects[i] += item.effects[i];
			}
		}
	});

	Session.set('hero.damage', calcDamageFromEffects(effects));
	Session.set('hero.ehp', calcEhpFromEffects(effects));

	Session.set('hero.aspd', calcAspdFromEffects(effects));

	SessionAmplify.set('hero.effects', effects);

	//calc crit chacne/damage
	var critChance = calcCritChance(effects.crit);
	var critDamage = calcCritDamage(effects.critDmg);

	Session.set('critChance', critChance);
	Session.set('critDamage', critDamage);
});

generateItemName = function (item, options) {
	//console.log(arguments.callee.toString());
	ResetMyRandom(item.nameSeed);

	var language = SessionAmplify.get('language');

	var name = MyRandom.choice(i18n[language].itemnames[item.type]);

	var nameSuffixRegex = /#(\S+)#/g;

	var result = null;
	while ((result = nameSuffixRegex.exec(name))) {
		name = name.replace(
			result[0],
			MyRandom.choice(i18n[language].itemnames.affixes[result[1]])
		)
	}

	if (item.bonus > 0) {
		name += " +"+item.bonus;
	}

	return name;
}
Handlebars.registerHelper("generateItemName", generateItemName);

itemDamageChange = function (item, options) {
	var effects = SessionAmplify.get('hero.effects');
	var currentItem = SessionAmplify.get('equipment.' + item.type);
	var currentDamage = Session.get('hero.damage');
	
	if (currentItem) {
		//remove the old item effects
		for (var i in currentItem.effects) {
			effects[i] -= currentItem.effects[i];
		}	
	}

	//apply new item effects
	for (var i in item.effects) {
		effects[i] += item.effects[i];
	}

	var newDamage = calcDamageFromEffects(effects);

	return format(newDamage - currentDamage, options.hash.format);
}
Handlebars.registerHelper("itemDamageChange", itemDamageChange);

itemEhpChange = function (item, options) {
	var effects = SessionAmplify.get('hero.effects');
	var currentItem = SessionAmplify.get('equipment.' + item.type);
	var currentEhp = Session.get('hero.ehp');
	
	if (currentItem) {
		//remove the old item effects
		for (var i in currentItem.effects) {
			effects[i] -= currentItem.effects[i];
		}
	}
	
	//apply new item effects
	for (var i in item.effects) {
		effects[i] += item.effects[i];
	}

	var newEhp = calcEhpFromEffects(effects);

	return format(newEhp - currentEhp, options.hash.format);
}
Handlebars.registerHelper("itemEhpChange", itemEhpChange);
