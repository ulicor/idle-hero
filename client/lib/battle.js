timePerBattleTurn = 1000;

Session.setDefault('monster.id', null);
Session.setDefault('monster.level', 0);
Session.setDefault('monster.hpCurrent', 0);
Session.setDefault('monster.hpMax', 0);

possibleMonsters = [
	'pingu', 'bunny', 'snake', 'piggy', 'tree', 'mouse'
];

setNextAutoBattle = function (type) {
	var types = {
		brawling: 		0 , 
		loadGame: 		20,
		moveDungeon: 	20,
		newDungeon: 	30,
		dungeonBattle: 	40,
		dungeonDeath: 	60,
	}

	if (type === 'brawling' && !Session.equals('combatmode', 'brawling'))
		Session.set('combatmode', 'brawling');

	if (type !== 'brawling' && !Session.equals('combatmode', 'dungeon'))
		Session.set('combatmode', 'dungeon');

	Session.set('nextAutoBattle', types[type]);
}

setNextAutoBattle('loadGame');

initBattle = function (battleData) {
	var selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel');

	var choosenMonster = Random.choice(possibleMonsters);

	Session.set('monster.id', choosenMonster);

	Session.set('monster.level', selectedMonsterLevel);

	var monsterMaxHp = selectedMonsterLevel*64;

	Session.set('monster.hpCurrent', monsterMaxHp);
	Session.set('monster.hpMax', monsterMaxHp);
	Session.set('monster.effects', {
		accuracy : 0,
		evasion : 0,
	});

	//set hero life back to max on battle start
	var hpCurrent = Session.get('hero.hpMax')
	Session.set('hero.hpCurrent', hpCurrent);

	if (battleData) {
		battleData.hpCurrent = hpCurrent;
		battleData.monsterHpCurrent = monsterMaxHp;
	}
}

var initBattleData = function (battleData) {
	battleData.level = SessionAmplify.get('hero.level');
	battleData.effects = SessionAmplify.get('hero.effects');
	battleData.hpCurrent = Session.get('hero.hpCurrent');
	battleData.hitChance = Session.get('hitChance');
	battleData.critChance = Session.get('critChance');
	battleData.critDamage = Session.get('critDamage');

	battleData.selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel');
	battleData.monsterHpCurrent = Session.get('monster.hpCurrent');
	battleData.monsterHitChance = Session.get('monsterHitChance');

	battleData.expGain = battleData.goldGain = 0;

	battleData.monstersKilled = 0;
}

var doBattleTurn = function (battleData) {
	if (!battleData.skipMonster) {
		battleData.monsterDamage = calcMonsterDamage(battleData.level, battleData.effects, battleData.selectedMonsterLevel, battleData.monsterHitChance);
		if (battleData.monsterDamage !== -1) { //not missed
			battleData.hpCurrent -= battleData.monsterDamage;
		}
	}

	if (battleData.hpCurrent > 0) { //if we are still alive deal damage
		battleData.playerDamage = calcPlayerDamage(battleData.level, battleData.effects, battleData.hitChance, battleData.critChance, battleData.critDamage);

		if (battleData.playerDamage.damage !== -1) { //not missed
			battleData.monsterHpCurrent -= battleData.playerDamage.damage;

			if (battleData.monsterHpCurrent <= 0) { //if we killed the monster, process rewards and restart battle
				var expGain = 20 * calcExpModifierBasedOnLevel(battleData.selectedMonsterLevel, battleData.level);
				battleData.expGain += expGain;

				var goldGain = Math.pow(battleData.selectedMonsterLevel, 1.1);
				goldGain *= 1 + (battleData.effects.loot/100);
				battleData.goldGain += Math.ceil(goldGain);

				battleData.monstersKilled++;
			}
		}
	}
}

doBattle = function (click, turns, timeDiff) {
	if (typeof turns !== "number") turns=1;
	if (typeof turns !== "number") timeDiff=0;

	var battleData = {
		timeDiff: timeDiff,
		click: click,
	};

	turns = Math.ceil(Math.pow(turns, 0.95));

	//test if we are in a battle
	//if we arent yet test if enough ticks passed to start the next battle
	var monsterId = Session.get('monster.id');
	var nextAutoBattle = Session.get('nextAutoBattle');
	if (!monsterId) {
		if (nextAutoBattle > turns) {
			nextAutoBattle -= turns;
			Session.set('nextAutoBattle', nextAutoBattle);
			return;
		} else {
			turns -= nextAutoBattle;
			nextAutoBattle = 0;
			Session.set('nextAutoBattle', nextAutoBattle);
			if (nextAutoBattle === 0 && !Session.equals('combatmode', 'brawling')) {
				Session.set('combatmode', 'brawling');
				if (SessionAmplify.equals('page', 'combat') && timeDiff < 2500)
					showMessage('blender', 'message-text', {timeout: 10000, data: {text: 'idledBrawling'}});
			}
			initBattle(battleData);
		}
	}

	var aspd = Session.get('hero.aspd');

	initBattleData(battleData);

	var doShowBattleMessages = true;
	var playerDied = false;

	for (;turns > 0;turns--) {
		bezierXOffsetIndex = -1;
		for (var attackCounter=0; attackCounter<aspd; attackCounter++) {
			battleData.skipMonster = attackCounter>0;
			doBattleTurn(battleData);

			//if monster is dead, break out of the aspd loop
			if (battleData.monsterHpCurrent <= 0) {
				break;
			}

			if (doShowBattleMessages) {
				showBattleMessages(battleData);
			}
		}

		doShowBattleMessages = false;

		var tryRestartBattle = false;

		//player died
		if (battleData.hpCurrent <= 0) {
			playerDied = true; //we failed, do restart the dungeon after all battles have been completed
			tryRestartBattle = true; //in case this is offline progress (a lot of turns) still do the next battles
		}

		//monster died
		if (battleData.monsterHpCurrent <= 0) {
			tryRestartBattle = true;
		}


		if (tryRestartBattle) {
			if (nextAutoBattle === 0) {
				initBattle(battleData);
			} else {
				Session.set('monster.id', null);
				break;
			}
		}
	}

	if (playerDied) {
		if (battleData.selectedMonsterLevel > 1) {
			SessionAmplify.set('selectedMonsterLevel', battleData.selectedMonsterLevel-1);
		}
		if (nextAutoBattle > 0) {
			showMessage('blender', 'message-text', {timeout: 10000, data: {text:'killedByMonsterDungeon'}});
			setNextAutoBattle('dungeonDeath');
			initDungeon();
		}
	}

	//save the player and monster hp
	Session.set('monster.hpCurrent', Math.max(0, battleData.monsterHpCurrent));
	Session.set('hero.hpCurrent', Math.max(0, battleData.hpCurrent));

	var exp = SessionAmplify.get('hero.exp');
	var gold = SessionAmplify.get('hero.gold');

	battleData.levelUps = 0;

	if (battleData.expGain > 0) {
		exp += battleData.expGain;
		if (exp >= 100) {
			battleData.levelUps = Math.floor(exp/100);

			track({
				type: 'levelUp',
				level: battleData.level,
				levelUps: battleData.levelUps,
				turns: turns,
				selectedMonsterLevel: battleData.selectedMonsterLevel,
				gold: battleData.gold,
				effects: battleData.effects,
				click: click,
				baseEffects: SessionAmplify.get('hero.baseEffects'),
				clickCounter: SessionAmplify.get('clickCounter'),
			})

			SessionAmplify.set('hero.level', battleData.level+battleData.levelUps);
			showLevelUpMessage();

			exp %= 100;
		}

		SessionAmplify.set('hero.exp', exp);
	}

	if (battleData.goldGain > 0) {
		gold += battleData.goldGain;

		SessionAmplify.set('hero.gold', gold);
	}

	if (battleData.monstersKilled > 0) {
		SessionAmplify.set('monstersKilled', SessionAmplify.get('monstersKilled')+battleData.monstersKilled);
	}

	if (battleData.timeDiff > 300000) {
		showMessage('alert', 'welcomeBack', {
			onClose: function () {
					Session.set('welcomeBack', false);
				},
			data: battleData,
		})
	} 
}

var autoBattleTick = function () {
	var lastAutoBattleTick = SessionAmplify.get('lastAutoBattleTick');
	var currentTime = +new Date;
	if (lastAutoBattleTick === 0) {
		lastAutoBattleTick = currentTime-1;
	}

	var timeDiff = currentTime - lastAutoBattleTick;
	var ticksToProcess = Math.ceil(timeDiff/timePerBattleTurn);

	if (ticksToProcess<0) {
		ticksToProcess=0;
	}

	var player = SessionAmplify.get('playerSettings');
	if (typeof player.disableOfflineProgress !== "undefined" && player.disableOfflineProgress && timeDiff > 2000) {
		timeDiff = 2000;
		ticksToProcess = Math.ceil(timeDiff/timePerBattleTurn);
		SessionAmplify.set('lastAutoBattleTick', currentTime);
	} else {
		SessionAmplify.set('lastAutoBattleTick', lastAutoBattleTick + timePerBattleTurn*ticksToProcess);
	}

	doBattle(false, ticksToProcess, timeDiff);

	Meteor.setTimeout(autoBattleTick, timePerBattleTurn);
}

Meteor.setTimeout(function () {
	autoBattleTick();
}, 50);