Handlebars.registerHelper("obj2array", function (obj, options) {
	var array = [];
	for (var i in obj) {
		array.push({
			key: i,
			value: obj[i]
		})
	}

	return array;
});

// array to array for 'each' iterations fix
/** bug causing code, seems because of array-iteration
{{#each array}}
	<option value="{{.}}" {{#if SessionAmplifyEquals 'lastAcceptedQuest' . }}selected="selected"{{/if}} >
		{{format . format="ms"}}
	</option>
{{/each}}
*/
Handlebars.registerHelper("arr2array", function (arr, options) {
	var array = [];
	for (var i in arr) {
		array.push({
			key: i,
			value: arr[i],
		})
	}

	return array;
});