var lastAnimate = null;
Deps.autorun(function () {
	var hpCurrent = Session.get('monster.hpCurrent');
	var hpMax = Session.get('monster.hpMax');
	var percentage = (hpCurrent / hpMax) * 100;
	var exp = SessionAmplify.get('hero.exp');

	if (typeof hpCurrent === "undefined") {
		return;
	}

	var bar = $('.monsterHpBar');

	if (bar.length === 0) {
		return;
	}

	var currentTime = +new Date;
	var animDuration = 200;

	if (lastAnimate) {
		var lastAnimTimespan = currentTime-lastAnimate.time;

		if (lastAnimTimespan > 1100) {
			animDuration = 0;
		} else {
			animDuration = Math.min(animDuration, lastAnimTimespan);

			//sometimes the queue bilds up, i didnt find a better way to solve this problem then this
			//basicly substract the queue size from the aniamtion duration
			animDuration = Math.max(0, animDuration - ((bar.queue("fx")||[]).length) - 10);
		}
	}

	if (lastAnimate && percentage === 100 && exp !== lastAnimate.exp) {
		bar
		.animate({
			width: "0%"
		}, animDuration)
		.animate({
			width: "100%"
		}, 0)
	} else {
		bar
		.animate({
			width: percentage + "%"
		}, animDuration)
	}

	lastAnimate = {
		percentage: percentage,
		exp: exp,
		time: currentTime
	}
});