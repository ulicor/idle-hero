var defaultLanguage = 'en_GB';

Handlebars.registerHelper("i18n", function(template, key, options) {
	var language = SessionAmplify.get('language');

	if (typeof i18n[language][template] === 'undefined' || typeof i18n[language][template][key] === 'undefined') {
		alert('i18n ' + language + ' - ' + template + ' - ' + key + ' is not set');
		language = defaultLanguage;
	}

	var res = i18n[language][template][key];

	var formatOption = null;

	if (options.hash.format) {
		formatOption = options.hash.format;
		delete options.hash.format;
	}

	if (options.hash.formatHash) {
		var hashFormats = options.hash.formatHash.split(',');
		for (var i in hashFormats) {
			var splitted = hashFormats[i].split('|');
			options.hash[splitted[0]] = format(options.hash[splitted[0]], splitted[1]);
		}
		delete options.hash.formatHash;
	}

	res = placeholderString(res, options.hash);

	res = format (res, formatOption);

	return res;
});
