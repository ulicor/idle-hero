if (typeof i18n === "undefined") {
	i18n = {};
}

i18n.en_GB = {
	// template menu
	menu : {
		combat: 'Combat',
		tavern: 'Tavern',
		shop: 'Shop',
		inventar: 'Inventory',
	},
	// template footer
	footer : {
		contact: 'Contact',
		cheat : 'BETA-CHEATS',
		settings : 'Settings',
	},
	// template contact
	contact : {
		toptext : 'Contact us',
		description : 'how do you feel about this game and suggestions for improvements',
		name : 'How we should call you: ',
		contact : 'How we can contact you: ',
		title : 'Title: ',
		text : 'Your message: *',
		send : 'send',
	// contact errors
		error_empty : 'Please write some text',
	// contact success
		success_send : 'The form has been successfuly saved, thanks for your feedback',
	},
	// template settings
	settings : {
		toptext : 'Settings',
		description : 'where you can change (almost) everything',
		name : 'Your epic name: *',
		email : 'E-Mail: ',
		newsletter : 'You want some newsletter?',
		gender : 'Gender: *',
		male : 'male',
		female : 'female',
		update : 'update',
		disableOfflineProgress : 'disable offline progress',
	// settings errors
		error_name_empty : 'Choose an epic name!',
	// settings success
		success_name : 'What an awesome name',
		success_gender : 'What a change!',
		success_email : 'Email saved. We won\'t misuse it. Promise!',
		success_newsletter_true : 'You are now receiving newsletters',
		success_newsletter_false : 'No more newsletters, CHECK!',	
		success_disableOfflineProgress_true : 'Offline progress, disabled',
		success_disableOfflineProgress_false : 'Offline progress, enabled',
	},
	// template combat
	combat : {
		level: 'Level',
		nextselect: '>',
		prevselect: '<',
		brawling: 'Brawling',
		dungeon: 'Dungeon',
		getmonster: 'Set monster level',
		getdungeon: 'Set dungeon level',
		lv: 'Lv.',
	},
	// template monster
	monster : {
		lv: 'Lv.',
		hp: 'HP:',
	},
	//monster names
	monsterNames: {
		pingu: 'Killer Penguin',
		bunny: 'Zombie Bunny',
		snake: 'Geeky Snake',
		piggy: 'Bat Pig',
		tree: 'Yggdrasil',
		mouse: 'Evil Mouse',
	},
	// template shop
	shop : {
		statcompares: 'Stat changes: ',
		equipped : 'currently equipped: ',
		attack: 'attack',
		buy: 'Buy now for #cost# Gold',
		refresh: 'Next stock refresh:',
		buyrefresh : 'refresh shop for #cost# gold',
	},
	// template inventar
	inventar : {
		equip : 'equip',
		sell : 'sell for #cost# gold',
	},
	// items 
	item : {
		damage : 'damage',
		ehp : 'ehp',
	},
	// itemtypes
	itemtypes : {
		weapon : 'weapon',
		armour : 'armour',
		accessory : 'accessory',
	},
	// template tavern
	tavern : {
		acceptQuest : 'accept quest',
		cancelQuest : 'cancel quest',
		receiveQuestReward : 'Collect reward',
		remaining : 'time remaining: #time#',
		gold : '#gold# gold,',
		questLabel : 'Some feeble peasants',
		questReward : '- are cowering near the door, looking at you.<br /> "Please help us. We have no where to turn to. We don\'t have much, but you can have this ... " <br /> They put some shabby coins on the table: <br /> (depending on the duration)<br />',
		questDurationSelectLabel: 'How long do you want to spend on them?',
		potionsLabel: 'A suspicious guy waves at you',
		potionsText : '"Do you want to buy some potions? They are delicious and increase your awesomeness ... I can fix you up with some, but don\'t tell the barkeep ..." He pulls out some flasks with strange looking liquids. <br/>"Red for Life, Blue for Attack, Green for Defence. Or was it Red for Attack?"',
		buyEffect: 'Buy potions for #gold# gold',
	},
	// messaging
	message : {
		idledBrawling: '<h1>brawling</h1>You were inactive for a long time. The hero has started brawling.',
		welcomeBackMessage: 'While you were away for #timeDiff# you gained #levelUps# levels and #goldGain# gold!',
		killedByMonsterDungeon: 'The monster has killed you and you failed the dungeon. <br/>You have entered a new dungeon.<br/>Maybe you should consider fighting an easier dungeon level.',
		dungeonCompleteMessage: 'You completed the dungeon and received #levelUps# levels and #goldGain# gold as reward',
		newdungeon: 'You have entered a new level #level# dungeon.',
	},
	// template sidebar
	sidebar : {
		level: 'Level:',
		exp: 'Exp:',
		hp: 'HP:',
		attack: 'Attack:',
		defence: 'Defence:',
		gold: 'Gold:',
		title: 'Your statistics',
		damage: 'Average damage',
		ehp: 'Effective HP',
		aspd: 'Attacks at once:',
	},
	// effects display
	effects : {
		att: '+ #value# attack',
		crit: '+ #value#% critical hit chance',
		critDmg: '+ #value#% critical hit damage',
		accuracy: '+ #value# increased accuracy',
		aspd: '+ #value#% increased attackspeed',
		def: '+ #value# defence',
		hp: '+ #value# hitpoints',
		evasion: '+ #value# evasion rating',
		loot: '+ #value#% increased gold gain',
	},
	// format
	format : {
		exp : "#exp#% exp",
		levelAndExp : "#level# level and #exp#% exp",
		digital : ".",
		thausend : ",",
		endingChars: {
			1000000000000000: ' q',
			1000000000000: ' t',
			1000000000: ' b',
			1000000: ' m',
			1000: ' k',
		},
		seconds: "seconds",
		minutes: "minutes",
		hours: "hours",
		days: "days",
		weeks: "weeks",
	},
	// item name generation
	itemnames : {
		weapon: ['#prefix# Baselard #suffix#', '#prefix# Cinquedea #suffix#', '#prefix# Ear dagger #suffix#', '#prefix# Katar #suffix#', '#prefix# Mercygiver #suffix#', '#prefix# Poniard #suffix#', '#prefix# Rondel #suffix#', '#prefix# Scramasax #suffix#', '#prefix# Sgian #suffix#', '#prefix# Stiletto #suffix#', '#prefix# Dirk #suffix#', '#prefix# Anelace #suffix#', '#prefix# Parrying Dagger #suffix#', '#prefix# Seax #suffix#', '#prefix# Sai #suffix#', '#prefix# Arming sword #suffix#', '#prefix# Shortsword #suffix#', '#prefix# Zweihander #suffix#', '#prefix# Claymore #suffix#', '#prefix# Longsword #suffix#', '#prefix# Broadsword #suffix#', '#prefix# Falchion #suffix#', '#prefix# Flamberge #suffix#', '#prefix# Sabre #suffix#', '#prefix# Katana #suffix#', '#prefix# Battleaxe #suffix#', '#prefix# Club #suffix#', '#prefix# Flail #suffix#', '#prefix# Mace #suffix#', '#prefix# Flanged mace #suffix#', '#prefix# Pernach #suffix#', '#prefix# Shestopyor #suffix#', '#prefix# Maul #suffix#', '#prefix# Morningstar #suffix#', '#prefix# Quarterstaff #suffix#', '#prefix# Warhammer #suffix#', '#prefix# Bec de Corbin #suffix#', '#prefix# Horsemanspick #suffix#', '#prefix# Bludgeon #suffix#', '#prefix# Bardiche #suffix#', '#prefix# Bill #suffix#', '#prefix# Glaive #suffix#', '#prefix# Guisarme #suffix#', '#prefix# Halberd #suffix#', '#prefix# Lance #suffix#', '#prefix# Lochaber Axe #suffix#', '#prefix# Lucernehammer #suffix#', '#prefix# Mancatcher #suffix#', '#prefix# Pitchfork #suffix#', '#prefix# Partisan #suffix#', '#prefix# Pike #suffix#', '#prefix# Ranseur #suffix#', '#prefix# Sovnya #suffix#', '#prefix# Spetum #suffix#', '#prefix# Swordstaff #suffix#', '#prefix# Volgue #suffix#', '#prefix# War-scythe #suffix#', '#prefix# Bow #suffix#', '#prefix# Longbow #suffix#', '#prefix# English Longbow #suffix#', '#prefix# Recurve Bow #suffix#', '#prefix# Mongolbow #suffix#', '#prefix# Shortbow #suffix#', '#prefix# Arbalest #suffix#', '#prefix# Ballista #suffix#', '#prefix# Reapeating Crossbow #suffix#', '#prefix# Sling #suffix#', '#prefix# Franziska #suffix#', '#prefix# Nzappa zap #suffix#', '#prefix# Tomahawk #suffix#', '#prefix# Throwing Spear #suffix#', '#prefix# Shuriken #suffix#', '#prefix# Chakram #suffix#', '#prefix# Culverin #suffix#', '#prefix# Brass Knuckles #suffix#', '#prefix# Cestus #suffix#', '#prefix# Finger Knife #suffix#', '#prefix# Korean Fan #suffix#', '#prefix# Madu #suffix#', '#prefix# Pata #suffix#', '#prefix# Roman Scissor #suffix#', '#prefix# Tekko #suffix#', '#prefix# Tessen #suffix#', '#prefix# Emei Daggers #suffix#', '#prefix# Bilbo #suffix#', '#prefix# Bolo #suffix#', '#prefix# Cinquedea #suffix#', '#prefix# Khanjali #suffix#', '#prefix# Colichemarde #suffix#', '#prefix# Kama #suffix#', '#prefix# Gladius #suffix#', '#prefix# Swiss Dagger #suffix#', '#prefix# Xiphos #suffix#', '#prefix# Aikuchi #suffix#', '#prefix# Shikomizue #suffix#', '#prefix# Barong #suffix#', '#prefix# Talibon #suffix#', '#prefix# Kodachi #suffix#', '#prefix# Wakizashi #suffix#', '#prefix# Pinuti #suffix#', '#prefix# Kerala #suffix#', '#prefix# Backsword #suffix#', '#prefix# Cutless #suffix#', '#prefix# Dao #suffix#', '#prefix# Dha #suffix#', '#prefix# Dussack #suffix#', '#prefix# Kampilan #suffix#', '#prefix# Karabela #suffix#', '#prefix# Khopesh #suffix#', '#prefix# Kilij #suffix#', '#prefix# Klewang #suffix#', '#prefix# Krabi #suffix#', '#prefix# Kukri #suffix#', '#prefix# Liuyedao #suffix#', '#prefix# Mameluke #suffix#', '#prefix# Nimcha #suffix#', '#prefix# Piandao #suffix#', '#prefix# Pulwar #suffix#', '#prefix# Scimiter #suffix#', '#prefix# Shamshir #suffix#', '#prefix# Shashka #suffix#', '#prefix# Szabla #suffix#', '#prefix# Talwar #suffix#', '#prefix# Yanmaodao #suffix#', '#prefix# Chokuto #suffix#', '#prefix# Epee #suffix#', '#prefix# Espada Ropera #suffix#', '#prefix# Estoc #suffix#', '#prefix# Flamberge #suffix#', '#prefix# Flyssa #suffix#', '#prefix# Hwandudaedo #suffix#', '#prefix# Ida #suffix#', '#prefix# Jian #suffix#', '#prefix# Kaskara #suffix#', '#prefix# Katzbalger #suffix#', '#prefix# Khanda #suffix#', '#prefix# Ninjato #suffix#', '#prefix# Saingeom #suffix#', '#prefix# Seax #suffix#', '#prefix# Sidesword #suffix#', '#prefix# Spadroon #suffix#', '#prefix# Spatha #suffix#', '#prefix# Takoba #suffix#', '#prefix# Tsurugi #suffix#', '#prefix# Ulfberht #suffix#', '#prefix# Dotanuki #suffix#', '#prefix# Falx #suffix#', '#prefix# Katana #suffix#', '#prefix# Miao Dao #suffix#', '#prefix# Nandao #suffix#', '#prefix# Nihonto #suffix#', '#prefix# Panabas #suffix#', '#prefix# Tachi #suffix#', '#prefix# Uchigatana #suffix#', '#prefix# Boarsword #suffix#', '#prefix# Changdao #suffix#', '#prefix# Dadao #suffix#', '#prefix# Espadon #suffix#', '#prefix# Flame-Bladed Sword #suffix#', '#prefix# Great Sword #suffix#', '#prefix# Nagamaki #suffix#', '#prefix# Nodachi #suffix#', '#prefix# Otachi #suffix#', '#prefix# Wodao #suffix#', '#prefix# Zanbato #suffix#', '#prefix# Zhanmadao #suffix#', '#prefix# Aruval #suffix#', '#prefix# Bolo #suffix#', '#prefix# Falcata #suffix#', '#prefix# Golok #suffix#', '#prefix# Harpe #suffix#', '#prefix# Kopis #suffix#', '#prefix# Kora #suffix#', '#prefix# Machete #suffix#', '#prefix# Makhaira #suffix#', '#prefix# Dacian Falx #suffix#', '#prefix# Parang Pandit #suffix#', '#prefix# Sosun Pattah #suffix#', '#prefix# Yatagan #suffix#', '#prefix# Hook Sword #suffix#', '#prefix# Shotel #suffix#', '#prefix# Chicken Sickel #suffix#', '#prefix# Crowbill #suffix#', '#prefix# Elephant Goad #suffix#', '#prefix# Hakapik #suffix#', '#prefix# Mattock #suffix#', '#prefix# Adze #suffix#', '#prefix# Battleaxe #suffix#', '#prefix# Broadaxe #suffix#', '#prefix# Dane Axe #suffix#', '#prefix# Doloire #suffix#', '#prefix# Fu #suffix#', '#prefix# Hand Axe #suffix#', '#prefix# Hatchet #suffix#', '#prefix# Labrys #suffix#', '#prefix# Masakari #suffix#', '#prefix# Ono #suffix#', '#prefix# Palstava #suffix#', '#prefix# Sagaris #suffix#', '#prefix# Sparth Axe #suffix#', '#prefix# Tabarzin #suffix#', '#prefix# Vechevoral #suffix#', '#prefix# Aklys #suffix#', '#prefix# Cambuk #suffix#', '#prefix# Chui #suffix#', '#prefix# Returning Boomerang #suffix#', '#prefix# Eskrima Sticks #suffix#', '#prefix# Frying Pan #suffix#', '#prefix# Hammer #suffix#', '#prefix# Hanbo #suffix#', '#prefix# Jutte #suffix#', '#prefix# Kanabo #suffix#', '#prefix# Knobkierrie #suffix#', '#prefix# Kotiate #suffix#', '#prefix# Macana #suffix#', '#prefix# Mere #suffix#', '#prefix# Otsuchi #suffix#', '#prefix# Patu #suffix#', '#prefix# Bo #suffix#', '#prefix# Eku #suffix#', '#prefix# Lathi #suffix#', '#prefix# Naboot #suffix#', '#prefix# Shareeravadi #suffix#', '#prefix# Taiaha #suffix#', '#prefix# Atgeir #suffix#', '#prefix# Boarspear #suffix#', '#prefix# Dory #suffix#', '#prefix# Hasta #suffix#', '#prefix# Qiang #suffix#', '#prefix# Sibat #suffix#', '#prefix# Trident #suffix#', '#prefix# Yari #suffix#', '#prefix# Guan Dao #suffix#', '#prefix# Ji #suffix#', '#prefix# Naginata #suffix#', '#prefix# Pudao #suffix#', '#prefix# Sasumata #suffix#', '#prefix# Harpoon #suffix#', '#prefix# Pilum #suffix#', '#prefix# Verutum #suffix#', '#prefix# Javelin #suffix#', '#prefix# Jangchang #suffix#', '#prefix# Woomera #suffix#', '#prefix# Kunai #suffix#', '#prefix# Throwndart #suffix#', '#prefix# Swiss arrow #suffix#', '#prefix# Daikyu #suffix#', '#prefix# Gungdo #suffix#', '#prefix# Hankyu #suffix#', '#prefix# Chu ko Nu #suffix#', '#prefix# Gastraphetes #suffix#', '#prefix# Bolas #suffix#', '#prefix# Bullwhip #suffix#', '#prefix# Ropedart #suffix#'],
		armour: ['#prefix# Dou or dō #suffix#', '#prefix# kusazuri #suffix#', '#prefix# Sode #suffix#', '#prefix# Kote #suffix#', '#prefix# Kabuto #suffix#', '#prefix# Mengu #suffix#', '#prefix# Haidate #suffix#', '#prefix# Suneate #suffix#', '#prefix# Guruwa #suffix#', '#prefix# Nodowa #suffix#', '#prefix# Tate-eri #suffix#', '#prefix# Manju no wa #suffix#', '#prefix# Manchira #suffix#', '#prefix# Wakibiki #suffix#', '#prefix# Armoured zukin #suffix#', '#prefix# Kogake #suffix#', '#prefix# Jingasa #suffix#', '#prefix# Hachi gane #suffix#', '#prefix# Katabira #suffix#', '#prefix# Fundoshi #suffix#', '#prefix# Kyahan #suffix#', '#prefix# Hakama #suffix#', '#prefix# Shitagi #suffix#', '#prefix# Tabi #suffix#', '#prefix# Waraji #suffix#', '#prefix# Kutsu #suffix#', '#prefix# Yugake #suffix#', '#prefix# Kegutsu #suffix#', '#prefix# Sashimono #suffix#', '#prefix# Horo #suffix#', '#prefix# Agemaki #suffix#', '#prefix# Jirushi #suffix#', '#prefix# Datemono #suffix#', '#prefix# Yebire #suffix#', '#prefix# Tanko #suffix#', '#prefix# Keiko #suffix#', '#prefix# Aspis #suffix#', '#prefix# Buckler #suffix#', '#prefix# Heater shield #suffix#', '#prefix# Hoplon #suffix#', '#prefix# Hungarian Shield #suffix#', '#prefix# Ishlangu #suffix#', '#prefix# Kite Shield #suffix#', '#prefix# Scuta #suffix#', '#prefix# Targe #suffix#', '#prefix# Armet #suffix#', '#prefix# Aventail #suffix#', '#prefix# Barbute #suffix#', '#prefix# Bascinet #suffix#', '#prefix# Benty Grange Helmet #suffix#', '#prefix# Brocas Helm #suffix#', '#prefix# Cervelliere #suffix#', '#prefix# Close Helmet #suffix#', '#prefix# Combat helmet #suffix#', '#prefix# Coppergate Helmet #suffix#', '#prefix# Coventry Sallet #suffix#', '#prefix# Enclosed helmet #suffix#', '#prefix# Falling buffe #suffix#', '#prefix# Frog-mouth helm #suffix#', '#prefix# Great helm #suffix#', '#prefix# Horned helmet #suffix#', '#prefix# Hounskull #suffix#', '#prefix# Kettle hat #suffix#', '#prefix# Mempo #suffix#', '#prefix# Nasal helmet #suffix#', '#prefix# Sallet #suffix#', '#prefix# Visor #suffix#', '#prefix# Enarmes #suffix#', '#prefix# Guige #suffix#', '#prefix# Heater shield #suffix#', '#prefix# Hungarian shield #suffix#', '#prefix# Mantlet #suffix#', '#prefix# Pavise #suffix#', '#prefix# Plate armour #suffix#', '#prefix# Almain rivet #suffix#', '#prefix# Aventail #suffix#', '#prefix# Besagew #suffix#', '#prefix# Bevor #suffix#', '#prefix# Brassard #suffix#', '#prefix# Breastplate #suffix#', '#prefix# Brigandine #suffix#', '#prefix# Chausses #suffix#', '#prefix# Codpiece #suffix#', '#prefix# Corslet #suffix#', '#prefix# Couter #suffix#', '#prefix# Cuirass #suffix#', '#prefix# Cuirassier #suffix#', '#prefix# Cuisses #suffix#', '#prefix# Culet #suffix#', '#prefix# Demi-Lancer #suffix#', '#prefix# Faulds #suffix#', '#prefix# Gamberson #suffix#', '#prefix# Gendarme #suffix#', '#prefix# Gothic plate armour #suffix#', '#prefix# Greave #suffix#', '#prefix# Greenwich armour #suffix#', '#prefix# Jack of plate #suffix#', '#prefix# Lame #suffix#', '#prefix# Laminar armour #suffix#', '#prefix# Lance rest #suffix#', '#prefix# Maximillian armour #suffix#', '#prefix# Morion #suffix#', '#prefix# Pauldron #suffix#', '#prefix# Plackart #suffix#', '#prefix# Proofing #suffix#', '#prefix# Rerebrace #suffix#', '#prefix# Rondel #suffix#', '#prefix# Sabaton #suffix#', '#prefix# Sallet #suffix#', '#prefix# Spaulders #suffix#', '#prefix# Tassets #suffix#', '#prefix# Vambrace #suffix#', '#prefix# Vervelles #suffix#', '#prefix# White armour #suffix#'],
		accessory: ['#prefix# Copper ring #suffix#', '#prefix# brass ring #suffix#', '#prefix# Silver ring #suffix#', '#prefix# Gold ring #suffix#', '#prefix# Platinum ring #suffix#', '#prefix# Diamond ring #suffix#', '#prefix# Saphire ring #suffix#', '#prefix# Ruby ring #suffix#', '#prefix# Topaz ring #suffix#', '#prefix# Copper amulet #suffix#', '#prefix# Brass amulet #suffix#', '#prefix# Silver amulet #suffix#', '#prefix# Gold amulet #suffix#', '#prefix# Platinum amulet #suffix#', '#prefix# Diamond amulet #suffix#', '#prefix# Saphire amulet #suffix#', '#prefix# Topaz amulet #suffix#', '#prefix# Ruby amulet #suffix#', '#prefix# cotton belt #suffix#', '#prefix# leather belt #suffix#', '#prefix# silk belt #suffix#', '#prefix# Copper belt #suffix#', '#prefix# Brass belt #suffix#', '#prefix# Silver belt #suffix#', '#prefix# Gold belt #suffix#', '#prefix# Cotton cape #suffix#', '#prefix# leather cape #suffix#', '#prefix# silk cape #suffix#', '#prefix# amoured cape #suffix#', '#prefix# Cotton gloves #suffix#', '#prefix# leather gloves #suffix#', '#prefix# silk gloves #suffix#', '#prefix# Iron gloves #suffix#', '#prefix# Silver gloves #suffix#', '#prefix# Gold gloves #suffix#', '#prefix# Copper earring #suffix#', '#prefix# brass earring #suffix#', '#prefix# Silver earring #suffix#', '#prefix# Gold earring #suffix#', '#prefix# Platinum earring #suffix#', '#prefix# Diamond earring #suffix#', '#prefix# Saphire earring #suffix#', '#prefix# Ruby earring #suffix#', '#prefix# Topaz earring #suffix#'],
		affixes : {
			prefix: ['Invisible', 'Gruesome', 'Adorable', 'Adventurous', 'Agressive', 'Average', 'Beautiful', 'Bright', 'Clean', 'Cloudy', 'Colorful', 'Cute', 'Dark', 'Elegant', 'Fancy', 'Glamourous', 'Graceful', 'Grotesque', 'Long', 'Muddy', 'Shiny', 'Spotless', 'Stormy', 'Strange', 'Ugly', 'Unusual', 'Bad', 'Breakable', 'Careful', 'Crazy', 'Expensive', 'Famous', 'Fragile', 'Helpfull', 'Horrible', 'Important', 'Innocent', 'Modern', 'Outstanding', 'Poor', 'Powerful', 'Real', 'Super', 'Tough', 'Wild', 'Ashamed', 'Bad', 'Bewildered', 'Black', 'Blue', 'Creepy', 'Dangerous', 'Depressing', 'Broad', 'Colossal', 'Fat', 'Chubby', 'Crooked', 'Curved', 'Deep', 'Flat', 'High', 'Hollow', 'Low', 'Narrow', 'Round', 'Shallow', 'Skinny', 'Straight', 'Wide', 'Gigantic', 'Huge', 'Immence', 'Large', 'Little', 'Mammoth', 'Massive', 'Miniature', 'Petite', 'Puny', 'Short', 'Small', 'Tall', 'Tiny', 'Boiling', 'Breezy', 'Broken', 'Chilly', 'Cold', 'Curly', 'Damaged', 'Damp', 'Dirty', 'Dry', 'Fluffy', 'Freezing', 'Hot', 'Warm', 'Wet'],
			suffix: ['of Awesomeness', 'of Bunny killing', 'of the Adorable', 'of Adventures', 'of Agressivity', 'of Attractiveness', 'of Beauty', 'of Clear sight', 'of cloudy heaven', 'of dark night', 'of fancy fashion', 'of living', 'to die', 'of long live', 'of good quality', 'of bad quality', 'of best quality', 'of worst quality', 'of Power', 'of Might', 'of wealth', 'of fortune', 'of plentyfullness', 'of easyness', 'of the dead', 'of killing', 'of cleverness', 'of the stupid', 'of shyness', 'of the wandering man', 'of the wild', 'of happyness', 'of the white knight', 'of darkness', 'of scaryness', 'of lonelyness', 'of tiredness', 'of selfishness', 'of toughness', 'of worryness', 'of charm', 'of enchanting', 'of smiling', 'of wonders', 'of joy', 'of silent', 'of biling', 'of freezing', 'of broken soul'],
		},
	},
	questnames : ['Kill the princess and rescue the dragon', 'Fly to the moon and get some Moongrapes for the Evilqueen', 'Milk the flying cow. Well, find it first', 'Find some soup for the duke\'s salt', 'Find some Giant Snails for racings'],
};