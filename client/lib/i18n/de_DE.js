if (typeof i18n === "undefined") {
	i18n = {};
}

i18n.de_DE = {
// template menu
	menu : {
		combat: 'Kampf',
		tavern: 'Taverne',
		shop: 'Shop',
		inventar: 'Inventar',
	},
	// template footer
	footer : {
		contact: 'Kontakt',
		cheat : 'BETA-CHEATS',
		settings : 'Einstellungen',
	},
	// template contact
	contact : {
		toptext : 'Kontaktiere uns',
		description : 'und sage uns, wie du dieses Spiel findest.',
		name : 'Wie wir dich nennen sollen: ',
		contact : 'Wie wir dich erreichen können: ',
		title : 'Überschrift: ',
		text : 'Deine Nachricht: *',
		send : 'Senden',
	// contact errors
		error_empty : 'Bitte schreib etwas',
	// contact success
		success_send : 'Deine Nachricht wurde abgespeichert. Vielen Dank!',
	},
	// template settings
	settings : {
		toptext : 'Einstellungen',
		description : 'hier kannst du (fast) alles ändern',
		name : 'Dein epischer Name: *',
		email : 'E-Mail: ',
		newsletter : 'You want some newsletter?',
		gender : 'Geschlecht: *',
		male : 'männlich',
		female : 'weiblich',
		update : 'Update',
		disableOfflineProgress : 'Offline Progress deaktivieren',
	// settings errors
		error_name_empty : 'Such einen epischen Namen aus!',
	// settings success
		success_name : 'Großartiger Name',
		success_gender : 'Was für eine gewaltige Veränderung!',
		success_email : 'Email saved. We won\'t misuse it. Promise!',
		success_newsletter_true : 'You are now receiving newsletters',
		success_newsletter_false : 'No more newsletters, CHECK!',	
		success_disableOfflineProgress_true : 'Offline progress, disabled',
		success_disableOfflineProgress_false : 'Offline progress, enabled',
	},
	// template combat
	combat : {
		level: 'Level',
		nextselect: '>',
		prevselect: '<',
		brawling: 'Herumstreuen',
		dungeon: 'Dungeon',
		getmonster: 'Monster suchen',
		getdungeon: 'Dungeon suchen',
		lv: 'Lv.',
	},
	// template monster
	monster : {
		lv: 'Lv.',
		hp: 'HP:',
	},
	//monster names
	monsterNames: {
		pingu: 'Tödlicher Pinguin',
		bunny: 'Untotes Kaninchen',
		snake: 'Brillenschlange',
		piggy: 'Bat Schwein',
		tree: 'Yggdrasil',
		mouse: 'Böse Maus',
	},
	// template shop
	shop : {
		statcompares: 'Stat changes: ',
		equipped : 'aktuell angelegt: ',
		attack: 'Angriff',
		buy: 'Jetzt für #cost# Gold kaufen',
		refresh: 'Nächstes Shopupdate:',
		buyrefresh : 'Shopupdate für #cost# Gold kaufen',
	},
	// template inventar
	inventar : {
		equip : 'anlegen',
		sell : 'Für #cost# Gold verkaufen',
	},
	// items 
	item : {
		damage : 'Schaden',
		ehp : 'eHP',
	},
	// itemtypes
	itemtypes : {
		weapon : 'Waffe',
		armour : 'Rüstung',
		accessory : 'Accessoir',
	},
	// template tavern
	tavern : {
		acceptQuest : 'Quest annehmen',
		cancelQuest : 'Quest abbrechen',
		receiveQuestReward : 'Belohnungen abholen',
		remaining : 'Restdauer: #time#',
		gold : '#gold# Gold,',
		questLabel : 'Some feeble peasants',
		questReward : '- are cowering near the door, looking at you.<br /> "Please help us. We have no where to turn to. We don\'t have much, but you can have this ... " <br /> They put some shabby coins on the table: <br /> (depending on the duration)',
		questDurationSelectLabel: 'How long do you want to help the peasents?',
		potionsLabel: 'A suspicious guy waves at you',
		potionsText : '"Do you want to buy some potions? They are delicious and increase your awesomeness ... I can fix you up with some, but don\'t tell the barkeep ..." He pulls out some flasks with strange looking liquids. "Red for Life, Blue for Attack, Green for Defence. Or was it Red for Attack?"',
		buyEffect: 'Buy potions for #gold# gold',
	},
	// messaging
	message : {
		idledBrawling: '<h1>Herumstreichen</h1>Du warst zu lange inaktiv. Dem Helden wurde es langweilig und sucht nun selbst Gegner.',
		welcomeBackMessage: 'Seitdem du dich zuletzt vor #timeDiff# angemeldet hast hast du #levelUps# Levels und #goldGain# Gold erhalten!',
		killedByMonsterDungeon: 'Das Monster hat dich getötet. <br/>Du hast einen neuen Dungeon betreten.<br/>Vielleicht solltest du einen leichteren Dungeon bekämpfen.',
		dungeonCompleteMessage: 'Du hast diesen Dungeon erfolgreich beendet und als Belohnung #levelUps# Level und #goldGain# Gold bekommen.',
		newdungeon: 'Du hast einen neuen Level #level# Dungeon betreten.',
	},
	// template sidebar
	sidebar : {
		level: 'Level:',
		exp: 'Exp:',
		hp: 'Leben:',
		attack: 'Angriff:',
		defence: 'Verteidigung:',
		gold: 'Gold:',
		title: 'Deine Statistiken',
		damage: 'Durchschnittlicher Schaden',
		ehp: 'Effektive HP',
		aspd: 'Angriffe / s',
	},
	// effects display
	effects : {
		att: '+ #value# Angriff',
		crit: '+ #value#% kritische Trefferchance',
		critDmg: '+ #value#% kritischer Trefferschaden',
		accuracy: '+ #value# erhöhte Trefferwertung',
		aspd: '+ #value#% Angriffstempo',
		def: '+ #value# Verteidigung',
		hp: '+ #value# Lebenspunkte',
		evasion: '+ #value# Ausweichwertung',
		loot: '+ #value#% mehr Gold',
	},
	// format
	format : {
		exp : "#exp#% exp",
		levelAndExp : "#level# level and #exp#% exp",
		digital : ",",
		thausend : ".",
		endingChars: {
			1000000000000000: ' Brd',
			1000000000000: ' Bio',
			1000000000: ' Mrd',
			1000000: ' Mio',
			1000: ' K',
		},
		seconds: "Sekunden",
		minutes: "Minuten",
		hours: "Stunden",
		days: "Tage",
		weeks: "Wochen",
	},
	// item name generation
	itemnames : {
	weapon: ['#prefix# Kurzschwert #suffix#', '#prefix# Langschwert #suffix#', '#prefix# Breitschwert #suffix#', '#prefix# Bastardschwerd #suffix#', '#prefix# Zweihandschwert #suffix#', '#prefix# Kurzdolch #suffix#', '#prefix# Langdolch #suffix#', '#prefix# Kurzspeer #suffix#', '#prefix# Langspeer #suffix#', '#prefix# Pike #suffix#', '#prefix# Ahlspieß #suffix#', '#prefix# Armbrust #suffix#', '#prefix# Arbalest #suffix#', '#prefix# Kurzbogen #suffix#', '#prefix# Langbogen #suffix#', '#prefix# Claymore #suffix#', '#prefix# Drechflegel #suffix#', '#prefix# Falarika #suffix#', '#prefix# Falchion #suffix#', '#prefix# Flamberge #suffix#', '#prefix# Flegel #suffix#', '#prefix# Flügellanze #suffix#', '#prefix# Franziska #suffix#', '#prefix# Glefe #suffix#', '#prefix# Guisarme #suffix#', '#prefix# Hakenspieß #suffix#', '#prefix# Halbmondpike #suffix#', '#prefix# Hellebarde #suffix#', '#prefix# Hippe #suffix#', '#prefix# Keule #suffix#', '#prefix# Kompositbogen #suffix#', '#prefix# Krähenfuß #suffix#', '#prefix# Kriegsgabel #suffix#', '#prefix# Kriegshammer #suffix#', '#prefix# Kriegssense #suffix#', '#prefix# Langmesser #suffix#', '#prefix# Messer #suffix#' , '#prefix# Lanze #suffix#', '#prefix# Madfaa #suffix#', '#prefix# Mangonel #suffix#', '#prefix# Menschenfänger #suffix#', '#prefix# Mordaxt #suffix#', '#prefix# Morgenstern #suffix#', '#prefix# Nierendolch #suffix#', '#prefix# Ohrendolch #suffix#', '#prefix# Panzerbrecher #suffix#', '#prefix# Panzerhandschuh #suffix#', '#prefix# Parierdolch #suffix#', '#prefix# Partisane #suffix#', '#prefix# Rabenschnabel #suffix#', '#prefix# Reiterhammer #suffix#', '#prefix# Rennspieß #suffix#', '#prefix# Richtschwert #suffix#', '#prefix# Ritterschwert #suffix#', '#prefix# Rossschinder #suffix#', '#prefix# Rutte #suffix#', '#prefix# Sägeschwert #suffix#', '#prefix# Scheibendolch #suffix#', '#prefix# Schleuder #suffix#', '#prefix# Schwebescheiben #suffix#', '#prefix# Degen #suffix#', '#prefix# Schwertstab #suffix#', '#prefix# Spetum #suffix#', '#prefix# Spieß #suffix#', '#prefix# Streitaxt #suffix#', '#prefix# Streitkolben #suffix#', '#prefix# Sturmspieß #suffix#', '#prefix# Bajonett #suffix#', '#prefix# Beil #suffix#', '#prefix# Cestus #suffix#', '#prefix# Drusus #suffix#', '#prefix# Entenschnabel #suffix#', '#prefix# Entermesser #suffix#', '#prefix# Gaffel #suffix#', '#prefix# Geißel #suffix#', '#prefix# Haken #suffix#', '#prefix# Kama #suffix#', '#prefix# Garotte #suffix#', '#prefix# Klampe #suffix#', '#prefix# Klauen-Armschienen #suffix#', '#prefix# Klingenstifel #suffix#', '#prefix# Kukri #suffix#', '#prefix# Kukri-Gama #suffix#', '#prefix# Leichte Kriegshacke #suffix#', '#prefix# Leichter Hammer #suffix#', '#prefix# Leichter Streitkolben #suffix#', '#prefix# Main-Gauche #suffix#', '#prefix# Nunchaku #suffix#', '#prefix# Sai #suffix#', '#prefix# Siangham #suffix#', '#prefix# Sichel #suffix#', '#prefix# Stachelhandschuh #suffix#', '#prefix# Stilett #suffix#', '#prefix# Stoßdolch #suffix#', '#prefix# Totschläger #suffix#', '#prefix# Wakizashi #suffix#', '#prefix# Wurfbeil #suffix#', '#prefix# Dreizack #suffix#', '#prefix# Katana #suffix#', '#prefix# Khopesh #suffix#', '#prefix# Krummsäbel #suffix#', '#prefix# Peitsche #suffix#', '#prefix# Rapier #suffix#', '#prefix# Säbel #suffix#', '#prefix# Schwere Kriegshacke #suffix#', '#prefix# Schwerer Stritkolben #suffix#', '#prefix# Streithammer #suffix#', '#prefix# Zwergische Streitaxt #suffix#', '#prefix# Bo #suffix#', '#prefix# Doppelklingenschwert #suffix#', '#prefix# Wurfpfeile #suffix#', '#prefix# Kette #suffix#', '#prefix# Krummschwert #suffix#', '#prefix# Naginata #suffix#', '#prefix# Orkische Doppelaxt #suffix#', '#prefix# Ranseur #suffix#', '#prefix# Schlegel #suffix#', '#prefix# Schreckensflegel #suffix#', '#prefix# Schwerer Streitflegel #suffix#', '#prefix# Stachelkette #suffix#', '#prefix# Tetsubo #suffix#', '#prefix# Zweihandkeule #suffix#', '#prefix# Blasrohr #suffix#', '#prefix# Bola #suffix#', '#prefix# Chakram #suffix#', '#prefix# Daikyu #suffix#', '#prefix# Hakenarmbrust #suffix#', '#prefix# Handarmbrust #suffix#', '#prefix# Lasso #suffix#', '#prefix# Leichte Repetierarmbrust #suffix#', '#prefix# Kampfstab #suffix#', '#prefix# Wurfspeer #suffix#', '#prefix# Kampfstab #suffix#'],
		armour: ['#prefix# Leinenrobe #suffix#', '#prefix# Baumwollrobe #suffix#', '#prefix# Wollrobe #suffix#', '#prefix# Seidenrobe #suffix#', '#prefix# Spinnenrobe #suffix#', '#prefix# Greifenlederweste #suffix#', '#prefix# Greifenlederrüstung #suffix#', '#prefix# Wildkatzenlederweste #suffix#', '#prefix# Wildkatzenlederrüstung #suffix#', '#prefix# Wolfslederweste #suffix#', '#prefix# Wolfslederrüstung #suffix#', '#prefix# Wildschweinlederrüstung #suffix#', '#prefix# Wildschweinlederweste #suffix#', '#prefix# Raptorenlederweste #suffix#', '#prefix# Raptorenlederrüstung #suffix#', '#prefix# Drachenlederweste #suffix#', '#prefix# Drachenlederrüstung #suffix#', '#prefix# Basiliskenlederweste #suffix#', '#prefix# Basiliskenlederrüstung #suffix#', '#prefix# Zigenlederweste #suffix#', '#prefix# Zigenlederrüstung #suffix#', '#prefix# Kupferrüstung #suffix#', '#prefix# Kupferplatte #suffix#', '#prefix# Bronzerüstung #suffix#', '#prefix# Bronzeplatte #suffix#', '#prefix# Eisenrüstung #suffix#', '#prefix# Eisenplatte #suffix#', '#prefix# Silberrüstung #suffix#', '#prefix# Silberplatte #suffix#', '#prefix# Goldrüstung #suffix#', '#prefix# Goldplatte #suffix#', '#prefix# Mithrilrüstung #suffix#', '#prefix# Mithrilplatte #suffix#', '#prefix# Diamantrüstung #suffix#', '#prefix# Diamantplatte #suffix#', '#prefix# Obsidianrüstung #suffix#', '#prefix# Obsidianplatte #suffix#', '#prefix# Adamantitrüstung #suffix#', '#prefix# Adamantitplatte #suffix#', '#prefix# Elementiumrüstung #suffix#', '#prefix# Elementiumplatte #suffix#'],
		accessory: ['#prefix# Kupferring #suffix#', '#prefix# Bronzering #suffix#', '#prefix# Silberring #suffix#', '#prefix# Goldring #suffix#', '#prefix# Platingring #suffix#', '#prefix# Diamantring #suffix#', '#prefix# Saphierring #suffix#', '#prefix# Rubinring #suffix#', '#prefix# Topazring #suffix#', '#prefix# Kupferamulett #suffix#', '#prefix# Bronzeamulett #suffix#', '#prefix# Silberamulett #suffix#', '#prefix# Goldamulett #suffix#', '#prefix# Platinamulett #suffix#', '#prefix# Diamantamulett #suffix#', '#prefix# Saphieramulett #suffix#', '#prefix# Topazamulet #suffix#', '#prefix# Rubinamulett #suffix#', '#prefix# Baumwollgürtel #suffix#', '#prefix# Ledergürtel #suffix#', '#prefix# Seidengürtel #suffix#', '#prefix# Kupfergürtel #suffix#', '#prefix# Bronzegürtel #suffix#', '#prefix# Silbergürtel #suffix#', '#prefix# Goldgürtel #suffix#', '#prefix# Baumwollumhang #suffix#', '#prefix# Lederumhang #suffix#', '#prefix# Seiderumhang #suffix#', '#prefix# Gepanzerter Umhang #suffix#', '#prefix# Wollhandschuhe #suffix#', '#prefix# Lederhandschuhe #suffix#', '#prefix# Seidenhandschuhe #suffix#', '#prefix# Eisenhandschuhe #suffix#', '#prefix# Silberhandschuhe #suffix#', '#prefix# Goldhandschuhe #suffix#', '#prefix# Kupferohrring #suffix#', '#prefix# Bronzeohrring #suffix#', '#prefix# Silberohrring #suffix#', '#prefix# Goldohrring #suffix#', '#prefix# Platinohrring #suffix#', '#prefix# Diamantohrring #suffix#', '#prefix# Saphierohrring #suffix#', '#prefix# Rubinohrring #suffix#', '#prefix# Topasohrring #suffix#'],
		affixes : {
			prefix: ['Unsichtbare', 'Grausame', 'Animalische', 'Anmutige', 'Anspruchsvolle', 'Anziehende', 'Atemberaubende', 'Athletische', 'Außergewöhnliche', 'Bedeutende', 'Beeindruckende', 'Beflügelte', 'Befreiende', 'Begehrenswerte', 'Beglückende', 'Belebend', 'Berauschend', 'Bewundernswerte', 'Bezaubernde', 'Brilliante', 'Dominante', 'Dynamische', 'Edele', 'Einfühlsame', 'Elegante', 'Entzückende', 'Erfrischende', 'Erhellende', 'Erotische', 'Erstaunliche', 'Erstklassige', 'Extravagante', 'Exzellente', 'Fabelhafte', 'Fantastische', 'Feine', 'Fesselnde', 'Feurige', 'Frische', 'Geheime', 'Geheimnissvolle', 'Geschmackvolle', 'Gigantische', 'Glänzende', 'Grandiose', 'Grenzenlose', 'Großartige', 'Heißblütige', 'Herrliche', 'Hervorragende', 'Hübsche', 'Ideale', 'Imponierende', 'Instinktive', 'Intelligente', 'Intensive', 'Komfortabele', 'Königliche', 'Kostbare', 'Kraftvolle', 'Lebendige', 'Lebhafte', 'Leuchtende', 'Lüsterne', 'Luxuriöse', 'Mächtige', 'Magische', 'Märchenhafte', 'Maximale', 'Mitreißende', 'Mysteriöse', 'Mystische', 'Packende', 'Perfekte', 'Phänomenale', 'Phantastische', 'Pikante', 'Positive', 'Potente', 'Prächtige', 'Pralle', 'Rasante', 'Reale', 'Reiche', 'Reine', 'Reizende', 'Riesige', 'Riskante', 'Romantische', 'Schamlose', 'Schöne', 'Seltene', 'Sensationelle', 'Sensibele', 'Sinnliche', 'Spannende', 'Spektakuläre', 'Spürbare', 'Starke', 'Stilvolle', 'Stürmische', 'Sündige', 'Traumhafte', 'Überlegende', 'Überwältigende', 'Unfassbare', 'Unglaubliche', 'Unsterbliche', 'Unwiederstehliche', 'Verblüffende', 'Verführerische', 'Verlockende', 'Vitale', 'Warme', 'Wertvolle', 'Wilde', 'Wohlklingende', 'Wohlrichende', 'Wunderbare', 'Wunderschöne', 'Zärtliche', 'Zuverlässige'],
			suffix: ['der Unsichtbarkeit', 'der Grausamkeit', 'des Animalismus', 'der Anmutigkeit', 'des Anspruchs', 'der Athletik', 'der Aussergewöhnlichkeit', 'der Bedeutsamkeit', 'der Beeindruckung', 'der Beflügelung', 'der Befreiung', 'des Begehrens', 'der Beglückung', 'des Belebens', 'des Rausches', 'der Bewunderung', 'der Brillianz', 'der Dominanz', 'der Dynamik', 'des Edelen', 'der Einfühlsamkeit', 'der Eleganz', 'der Entzückung', 'der Erfrischung', 'des Erhellens', 'der Erotik', 'des Erstaunens', 'der Erstklassigkeit', 'der Extravaganz', 'der Exellenz', 'der Fabelhaftigkeit', 'der Fantastigkeit', 'der Feine', 'des Fesselns', 'der Feurigkeit', 'der Frische', 'des Geheimen', 'des Geheimnissvollen', 'der Gigantik', 'des Glanzes', 'der Grenzenlosigkeit', 'der Großartigkeit', 'des Heißblutes', 'der Herrlichkeit', 'des Ideals', 'der Imposanz', 'der Instinkte', 'der Intelligenz', 'der Intensivität', 'des Komforts', 'des Königs', 'der Kostbarkeit', 'der Kraft', 'des Lebens', 'der Lebhaftigkeit', 'des Leuchtens', 'der Lüsternheit', 'der Luxeriösität', 'der Macht', 'der Magie', 'des Märchens', 'des Mitreißens', 'der Mysterien', 'der Mystik', 'des Packens', 'der Perfektion', 'der Phänomene', 'der Phantasie', 'der Pikanz', 'des Reichtums', 'der Reinlichkeit', 'des Reizes', 'der Riesen', 'des Risikos', 'der Romantik', 'der Schamlosigkeit', 'des Schams', 'der Schönheit', 'der Seltenheit', 'der Sensation', 'der Sensibilität', 'der Sinnlichkeit', 'des Sturms', 'der Sünde', 'des Traums', 'des Überlebens', 'der Überwältigung', 'der Unfassbarkeit', 'der Unsterblichkeit', 'der Unsterblichkeit', 'der Unwiederstehlichkeit', 'des Verblüffens', 'der Verführung', 'der Verlockung', 'der Vitalität', 'der Wärme', 'des Werts', 'der Wildheit', 'des Wohlklangs', 'der Wunder', 'der Zärtlichkeit', 'der Zuverlässigkeit'],
		},
	},
	questnames : ['Besorge eine Trollsocke für die Trollsockensuppe']

};