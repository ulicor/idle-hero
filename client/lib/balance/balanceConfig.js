config = {
	// version
	version : '0.1.7',

	// shop
	stockRefreshRate : 5*60*1000, // in ms
	stockRefreshBase : 10,
	stockRefreshBaseCost : 1,
	shopItemCount : 5,

	// quest
	questExpBaseMultiplier : 1/6000,
	questExpExponent : 0.5,
	questExpMultiplier : 25,
	questGoldBaseMultiplier : 1/6000,
	questGoldExponent : 0.5,
	questGoldMultiplier : 100,

	// effects
	hitChanceZero : 75,
	hitChanceMultiplier : 50,
	hitChanceOffset : 1,
	hitChanceScalar : 50,
	accuracyDivisor : 1000,

}