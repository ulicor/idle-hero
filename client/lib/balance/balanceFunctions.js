exponetialGain = function (base, basemultiplier, exponent, multiplier) {
	return Math.pow(base * basemultiplier, exponent) * multiplier;
}

logarithmicGain = function (base, basemultiplier, logbase, multiplier) {
	return Math.log(base * basemultiplier) * multiplier / Math.log(logbase);
}

getHitChance = function (attackerLevel, defenderLevel, attackerAccuracy, defenderEvasion) {
	//leveldiff = monsterlevel - playerlevel;

	leveldiff = defenderLevel - attackerLevel;
	var h0 = config.hitChanceZero;
	var multiplier = config.hitChanceMultiplier;
	var scalar = config.hitChanceScalar;
	var multOffset = config.hitChanceOffset;

	// level
	var h = offsetAtan(h0, leveldiff, multiplier, multOffset, scalar);

	if (h < 0) h = 0;
	if (h > 100) h = 100;

	// accuracy and evasion
	var effectiveAccuracy = (attackerAccuracy - defenderEvasion)/config.accuracyDivisor;
	if (effectiveAccuracy < 0 ) effectiveAccuracy = 0
	h1 = 100 - (98 - h) * Math.pow(0.5, effectiveAccuracy) + 1;


	return h1;
}

calcDamageFromEffects = function (effects) {
	var critMult = 1 + calcCritDamage(effects.critDmg) * calcCritChance(effects.crit);
	var hitMult = Math.min(
		100, 
		calcHitChanceFromEffects(
			effects,
			SessionAmplify.get('hero.level'),
			Session.get('monster.effects'),
			SessionAmplify.get('selectedMonsterLevel')
		)
	) / 100;

	var aspd = calcAspdFromEffects(effects);

	return Math.ceil(effects.att) * critMult * hitMult * aspd;
}

calcEhpFromEffects = function (effects) {
	var level = SessionAmplify.get('hero.level');
	var selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel');
	var monstereffects = Session.get('monster.effects');

	var monsterHitMult = Math.min(
		100, 
		calcHitChanceFromEffects(
			monstereffects,
			SessionAmplify.get('selectedMonsterLevel'),
			effects,
			SessionAmplify.get('hero.level')
		)
	) / 100;

	var defMult = calcDefDamageReduction(effects.def, level);

	return (SessionAmplify.get('hero.level')*5 + effects.hp) / monsterHitMult / defMult;
}

calcHitChanceFromEffects = function (attackerEffects, attackerLevel, defenderEffects, defenderLevel) {
	var baseHitChance = 0.75;
	if (typeof defenderEffects === "undefined") defenderEffects = {accuracy : 0, evasion : 0};
	if (typeof attackerEffects === "undefined") attackerEffects = {accuracy : 0, evasion : 0};

	var hitChance = getHitChance (
		attackerLevel, 
		defenderLevel, 
		attackerEffects.accuracy, 
		defenderEffects.evasion
	);

	return hitChance;
}

calcCritChance = function (critRating) {
	return Math.min(1, critRating/100 + 0.1);
}

calcCritDamage = function (critDamageRating) {
	return critDamageRating/100 + 0.5;
}

calcAspdFromEffects = function (effects) {
	return 1 + Math.min(9, Math.round(effects.aspd/100, 0.9));
}

calcDefDamageReduction = function (def, level) {
	return 1 / (1 + def/level);
}

calcMonsterBaseDamage = function (selectedMonsterLevel) {
	if (selectedMonsterLevel > 500) {
		return selectedMonsterLevel * 5 - 1598;
	} else if (selectedMonsterLevel > 100) {
		return selectedMonsterLevel * 2 - 98;
	} else {
		return selectedMonsterLevel + 2;
	}
}

calcMonsterDamage = function (level, effects, selectedMonsterLevel, monsterHitChance) {
	if (monsterHitChance < Math.random() * 100) { //missed
		return -1;
	}

	var baseAttack = calcMonsterBaseDamage(selectedMonsterLevel);

	return Math.ceil(baseAttack * calcDefDamageReduction(effects.def, level));
}

calcPlayerDamage = function (level, effects, hitChance, critChance, critDamage) {
	result = {};
	if (hitChance < Math.random() * 100) { //missed
		result.damage = -1;
	} else {
		result.damage = Math.ceil(effects.att)

		//calc crit
		result.criticalHit = critChance >= Math.random();
		if (result.criticalHit) { //we did a crit!
			result.damage = Math.ceil(result.damage * (1 + critDamage));
		}
	}

	return result;
}

calcExpModifierBasedOnLevel = function (selectedMonsterLevel, level) {
	var levelDiff = selectedMonsterLevel - level;
	if (levelDiff<0) {
		//adding a tolerance for the leveldiff of 10% of current level but at most 1000 or ofcourse the actual levelDiff
		levelDiff += Math.min(level/10, 1000, -levelDiff)
		return Math.pow(Math.E, levelDiff / 50);
	} else {
		return (2-Math.pow(Math.E, -levelDiff / 50));
	}
}

offsetAtan = function (x0, d, multiplier, offset, scalar) {
	var m = multiplier - offset;
	var d0 = - Math.tan( (multiplier - x0) * (Math.PI / 2) / m ) * scalar;

	var h = multiplier - Math.atan((d - d0) / scalar ) * m / (Math.PI / 2);
	return h;
}