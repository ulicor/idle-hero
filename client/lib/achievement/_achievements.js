achievements = {}

pushAchievement = function (key, achievement) {
	achievements[key] = achievement;
}

isAchievementUnlocked = function (achievmentId) {

}

unlockAchievement = function (achievmentId) {
	var achievement = achievements[achievmentId];
	if (typeof achievement === 'undefined') {
		return false;
	}
	isUnlocked = SessionAmplify.get('achievementUnlocked.'+achievmentId);

	if ((typeof isUnlocked === 'undefined' || !isUnlocked) 
		|| typeof achievement.multiple !== 'undefined' || achievement.multiple) {

		var unlock = achievement.unlock;
		var callback = function () {
			SessionAmplify.set('achievementUnlocked.'+achievmentId, true);
		}

		if (typeof unlock === 'function') {
			unlock({}, callback);
		} else {
			callback();
		}
	}
}