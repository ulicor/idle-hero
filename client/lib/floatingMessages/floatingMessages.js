showMessageFloatingMessage = function (message, className, startAnimation, endAnimation, duration) {
	var parent = $('#floatingMessagesParent');

	if (parent.length === 0) {
		return;
	}
	
	$('<div/>')
	.addClass('floatingMessage unselectable')
	.addClass(className)
	.text(message)
	.css(startAnimation)
	.appendTo(parent)
	.animate(endAnimation, duration, 'linear', function () {
		$(this).remove();
	})
}

bezierXOffsetIndex = -1;
bezierXOffsets = [0, 100, -100, 50, -50, 25, -25, 75, -75];
var getNextBezierXOffset = function () {
	bezierXOffsetIndex++;
	bezierXOffsetIndex %= bezierXOffsets.length;
	return bezierXOffsets[bezierXOffsetIndex];
}

showMessageFloatingMessageAboveMouse = function (message, className, startAnimation, endAnimation, duration) {
	startAnimation.top = mouseY;
	startAnimation.left = mouseX;
	startAnimation.position = 'absolute';

	endAnimation.opacity = 0;

	var xOffset = getNextBezierXOffset();
	var halvAbsOffset = Math.abs(xOffset/2);
	var startAngle, endAngle = 0;
	if (xOffset < 0) {
		startAngle = (360+halvAbsOffset)%360;
		endAngle = 360-halvAbsOffset;
	} else {
		startAngle = 360-halvAbsOffset;
		endAngle = (360+halvAbsOffset)%360;
	}
	endAnimation.path = new $.path.bezier({
		start: {
			x: mouseX,
			y: mouseY,
			angle: startAngle,
			length: 0.4
		},
		end: {
			x: mouseX+xOffset,
			y: mouseY-80,
			angle: endAngle,
          	length: 0.4
		}
	})

	showMessageFloatingMessage(
		message,
		className,
		startAnimation,
		endAnimation,
		1000
	)
}

showMessageFloatingMessageAboveMonster = function (message, className, startAnimation, endAnimation, duration) {
	var monster = $('#monster');

	if (monster.length) {
		var offset = monster.offset();
		var width = monster.width();
		var height = monster.height();

		var centerX = offset.left + width / 2;
		var centerY = offset.top + height / 2 + 20;

		startAnimation.top = centerY;
		startAnimation.left = centerX;
		startAnimation.position = 'absolute';

		endAnimation.opacity = 0;

		var xOffset = getNextBezierXOffset();
		var halvAbsOffset = Math.abs(xOffset/2);
		var startAngle, endAngle = 0;
		if (xOffset < 0) {
			startAngle = (360+halvAbsOffset)%360;
			endAngle = 360-halvAbsOffset;
		} else {
			startAngle = 360-halvAbsOffset;
			endAngle = (360+halvAbsOffset)%360;
		}
		endAnimation.path = new $.path.bezier({
			start: {
				x: centerX,
				y: centerY,
				angle: startAngle,
				length: 0.4
			},
			end: {
				x: centerX+xOffset,
				y: centerY-80,
				angle: endAngle,
	          	length: 0.4
			}
		})

		showMessageFloatingMessage(
			message,
			className,
			startAnimation,
			endAnimation,
			1000
		)
	}
}

showMessageFloatingMessageAbovePlayer = function (message, className, startAnimation, endAnimation, duration) {
	var player = $('#player');

	if (player.length) {
		var offset = player.offset();
		var width = player.width();
		var height = player.height();

		var centerX = offset.left + width / 2;
		var centerY = offset.top + height / 2 + 20;

		startAnimation.top = centerY;
		startAnimation.left = centerX;
		startAnimation.position = 'absolute';

		endAnimation.top = '-=80';
		endAnimation.opacity = 0;

		showMessageFloatingMessage(
			message,
			className,
			startAnimation,
			endAnimation,
			1000
		)
	}
}

showMessageFloatingMessageAbovePlayerExpBar = function (message, className, startAnimation, endAnimation, duration) {
	var playerExpBar = $('#playerExpBar');

	if (playerExpBar.length) {
		var offset = playerExpBar.offset();
		var width = playerExpBar.width();
		var height = playerExpBar.height();

		var centerX = offset.left + width / 2;
		var centerY = offset.top + height / 2 + 20;

		startAnimation.top = centerY;
		startAnimation.left = centerX;
		startAnimation.position = 'absolute';

		endAnimation.top = '-=50';
		endAnimation.opacity = 0;

		showMessageFloatingMessage(
			message,
			className,
			startAnimation,
			endAnimation,
			1000
		)
	}
}

showClickDamageMessage = function (damage, criticalHit) {
	var message = ''+damage;
	var className = 'damageMessage';
	if (criticalHit) {
		message += ' !';
		className += ' criticalDamageMessage';
	}

	showMessageFloatingMessageAboveMouse(
		message,
		className,
		{},
		{},
		1000
	)
}

showAutoDamageMessage = function (damage, criticalHit) {
	var message = ''+damage;
	var className = 'damageMessage';
	if (criticalHit) {
		message += ' !';
		className += ' criticalDamageMessage';
	}

	showMessageFloatingMessageAboveMonster(
		message,
		className,
		{},
		{},
		1000
	)
}

showClickMissedMessage = function () {
	var message = 'missed !';
	var className = 'missedMessage';

	showMessageFloatingMessageAboveMouse(
		message,
		className,
		{},
		{},
		1000
	)
}

showDamageTakenMessage = function (damage) {
	var message = ''+damage;
	var className = 'damageTakenMessage';

	showMessageFloatingMessageAbovePlayer(
		message,
		className,
		{},
		{},
		1000
	)
}

showPlayerDodgeMessage = function () {
	var message = 'dodge !';
	var className = 'playerDodgeMessage';

	showMessageFloatingMessageAbovePlayer(
		message,
		className,
		{},
		{},
		1000
	)
}

showPlayerDeathMessage = function () {
	var message = 'died !';
	var className = 'playerDeathMessage';

	showMessageFloatingMessageAbovePlayer(
		message,
		className,
		{},
		{},
		1000
	)
}

showAutoMissedMessage = function () {
	var message = 'missed !';
	var className = 'missedMessage';

	showMessageFloatingMessageAboveMonster(
		message,
		className,
		{},
		{},
		1000
	)
}

showLevelUpMessage = function () {
	var message = 'Level Up!';
	var className = 'levelUpMessage';
	
	showMessageFloatingMessageAbovePlayerExpBar(
		message,
		className,
		{},
		{},
		1000
	)

	showMessageFloatingMessageAbovePlayer(
		message,
		className,
		{},
		{},
		1000
	)
}

showBattleMessages = function (battleData) {
	//if we died show death message
	//if we survived show damage dealt and damage received message
	if (battleData.hpCurrent <= 0) {
		unlockAchievement('firstDeath');
		showPlayerDeathMessage();
	} else {
		if (!battleData.skipMonster) {
			if (battleData.monsterDamage === -1) { //missed
				showPlayerDodgeMessage();
			} else {
				showDamageTakenMessage(formatNumber(battleData.monsterDamage));
			}
		}

		if (battleData.playerDamage.damage === -1) { //missed
			if (battleData.click) {
				showClickMissedMessage();
			} else {
				showAutoMissedMessage();
			}
		} else {
			//display damage message
			if (battleData.click) {
				showClickDamageMessage(formatNumber(battleData.playerDamage.damage), battleData.playerDamage.criticalHit);
			} else {
				showAutoDamageMessage(formatNumber(battleData.playerDamage.damage), battleData.playerDamage.criticalHit);
			}
		}
	}
}