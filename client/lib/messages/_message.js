var messages = {
	onClose: {},
	timeout: {},
	timeoutHandler: {},
	hide: {},
	onrender: {},
	multiple: {},
};
var transition = 100;

var closeMessage = function (type, trans) {
	if (typeof trans === 'undefined')
		trans = transition;
	var hide = messages.hide[type];
	var callback = function () {
		Session.set('messageTemplate.'+type, false);
		var onCloseFunc = messages.onClose[type];
		if (typeof onCloseFunc === "function") {
			onCloseFunc();
		}
	}

	if (trans && typeof hide === 'function') {
		hide( callback, trans );
	} else {
		callback();
	}
}

var handleMultipleMessage = function (type) {
	var multiple = messages.multiple[type];
	var handletypes = {
		default : // overwrite the current one
			function () {
				closeMessage(type);
				return true;
			},
		queuing : // queue the messages
			function () {
				// nothing right here now ... laters
			},
	}
	if (typeof multiple === 'undefined' || typeof handletypes[multiple] === 'undefined') {
		multiple = 'default';
	}

	var func = handletypes[multiple];
	return func();
}

var getMessageTemplate = function (type) {
	var messageTemplate = Session.get('messageTemplate.' + type);
	var data = Session.get('messageTemplate.' + type + '.data');
	if (messageTemplate) {
		Template[messageTemplate].data = data;
		return Template[Session.get('messageTemplate.' + type)];
	} else {
		return false;
	}
}

showMessage = function (type, contentTemplate, options) {
	/*
	if ((Session.get('messageTemplate.'+type) && handleMultipleMessage(type))) // if there is an older message.
		console.log('blub'); //*/
	/*if (typeof messages.timeoutHandler[type] !== 'undefined'){
		closeMessage(type, 0);
		Meteor.clearTimeout(messages.timeoutHandler[type]);
		messages.timeoutHandler[type] = undefined;
	} //*/

	Session.set('messageTemplate.'+type, contentTemplate);
	if (typeof options === "undefined")
		options = {};

	messages.onClose[type] = options.onClose;
	messages.timeout[type] = options.timeout;
	messages.multiple[type] = options.multiple;
	Session.set('messageTemplate.' + type + '.data', options.data);

	var onrender = messages.onrender[type];
	if (typeof onrender === 'function') {
		onrender();
	}
}

//showMessage('notice'/'alert'/'blender', templatename, {onClose: function () {}, timeout: 5000});
//Meteor.setTimeout(function(){showMessage('notice', 'killedByMonsterDungeon');}, 3000);

Session.setDefault('messageTemplate.notice',false);
Session.setDefault('messageTemplate.alert',false);
Session.setDefault('messageTemplate.blender',false);


/**alert**/
messages.hide['alert'] = function (callback, trans) {
	$('#messageAlert').fadeOut(trans, callback);
}

Template.messageAlert.messageTemplate = function () {
	return getMessageTemplate('alert');
}

Template.messageAlert.events({
	'click #messageAlert-overlay': function (e,t) {
		e.preventDefault();
		
		closeMessage('alert');
	},
	'click #messageAlert-close': function (e,t) {
		e.preventDefault();
		
		closeMessage('alert');
	}
})

/**notice**/
messages.hide['notice'] = function (callback, trans) {
	var box = $('#messageNotice-box');
	box.animate({right: '-'+(box.width()+20)+'px' }, trans, callback);
}

messages.onrender['notice'] = function() {
	var timer = messages.timeout['notice'];
	if (typeof timer === 'undefined')
		timer = 5000;
	messages.timeoutHandler['notice'] = Meteor.setTimeout(function () {
		closeMessage('notice');
	}, timer);
}

Template.messageNotice.messageTemplate = function () {
	return getMessageTemplate('notice');
}

Template.messageNotice.events({
	'click #messageNotice-box': function (e,t) {
		e.preventDefault();
		
		closeMessage('notice');
	},
})


/**blender**/
messages.hide['blender'] = function (callback, trans) {
	$('#messageBlender-box').fadeOut(trans, callback);
}

messages.onrender['blender'] = function() {
	var timer = messages.timeout['blender'];
	if (typeof timer === 'undefined')
		timer = 5000;
	
	messages.timeoutHandler['blender'] = Meteor.setTimeout(function () {
		closeMessage('blender');
	}, timer);
}

Template.messageBlender.messageTemplate = function () {
	return getMessageTemplate('blender');
}

Template.messageBlender.events({
	'click #messageBlender-box, click #messageBlender-close': function (e,t) {
		e.preventDefault();
		
		closeMessage('blender');
	},
})

