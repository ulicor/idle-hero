Session.setDefault('combatmode', 'dungeon');

Template.combat.monsterName = function () {
	var language = SessionAmplify.get('language');
	var choosenMonster = Session.get('monster.id');

	return i18n[language].monsterNames[choosenMonster];
}

Template.combat.events({
	'click #enableBrawling': function (e) {
		var mode = $(e.target).attr('data-type');

		if (Session.get('combatmode') !== mode) {
			setNextAutoBattle('brawling');
			if (!Session.get('monster.id')) {
				initBattle();
			}

			Session.set('combatmode', mode);
		}
	},
	'click #enableDungeon': function (e) {
		var mode = $(e.target).attr('data-type');
		if (Session.get('combatmode') !== mode) {
			
			setNextAutoBattle('newDungeon');
			Session.set('monster.id', null);

			Session.set('combatmode', mode);
		}
	},
	/*'click .combatChoice' : function (e, t) {
		Session.set('combatmode', $(e.target).attr('data-type'));
	},*/
	'click #nextMonster': function (e) {
		e.preventDefault();

		var selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel')+1;

		SessionAmplify.set('selectedMonsterLevel', selectedMonsterLevel);

		var combatMode = Session.get('combatmode');
		
		var nextAutoBattle = Session.get('nextAutoBattle');
		if (nextAutoBattle === 0) {
			initBattle();
		} else {
			setNextAutoBattle('newDungeon');
			lazyInitDungeon();
		}
	},

	'click #prevMonster': function (e) {
		e.preventDefault();

		var selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel')-1;

		if (selectedMonsterLevel > 0) {
			SessionAmplify.set('selectedMonsterLevel', selectedMonsterLevel);

			var nextAutoBattle = Session.get('nextAutoBattle');
			if (nextAutoBattle === 0) {
				initBattle();
			} else {
				setNextAutoBattle('newDungeon');
				lazyInitDungeon();
			}
		}
	},
	'click #seekMonster': function (e, template) {
		e.preventDefault();

		var selectedMonsterLevel = parseInt(template.find("#seekMonsterLvl").value);

		if (selectedMonsterLevel > 0) {
			SessionAmplify.set('selectedMonsterLevel', selectedMonsterLevel);

			var nextAutoBattle = Session.get('nextAutoBattle');
			if (nextAutoBattle === 0) {
				initBattle();
			} else {
				setNextAutoBattle('newDungeon');
				lazyInitDungeon();
			}
		}
	}
})

