Session.setDefault('newShopStock', false);

var getBonusMultForEffect = function (effect, item) {
	if (effect === 'att' || effect === 'hp') {
		return item.level * 3;
	}

	if (effect === 'crit') {
		return 50 + (item.bonus * 5);
	}
	if (effect === 'critDmg') {
		return 50 + (item.bonus * 55);
	}
	if (effect === 'aspd') {
		return 50 + (item.bonus * 40);
	}

	if (effect === 'loot') {
		return 50 + (item.bonus * 10);
	}

	return item.level;
}

var createRandomEffects = function (item) {
	var effetcs = {};

	//random number 1-3
	var effectsCount = Math.ceil(MyRandom.fraction() * 3);

	var itemGuaranteedEffect = itemGuaranteedEffects[item.type];
	if (itemGuaranteedEffect) {
		effetcs[itemGuaranteedEffect] = item.level * (MyRandom.fraction() / 5 + 0.9);
		effectsCount--;
	}

	for (var i = 0; i<effectsCount; i++) {
		var selectedEffect = MyRandom.choice(possibleEffects[item.type]);
		var bonusMult = getBonusMultForEffect(selectedEffect, item);

		if (typeof effetcs[selectedEffect] === "undefined") {
			effetcs[selectedEffect] = 0;
		}

		effetcs[selectedEffect] += Math.ceil(1/effectsCount * bonusMult * (MyRandom.fraction() / 5 + 0.9));
	}

	return effetcs;
}

var defaultStockUpdate = {time : 0, count: 0 };
SessionAmplify.setDefault('lastBoughtStockRefresh', defaultStockUpdate);

var shopItemCount = config.shopItemCount;
var updateStock = function () {
	var lastBoughtStockRefresh = SessionAmplify.get('lastBoughtStockRefresh');
	var randSeed = (SessionAmplify.get('nextStockUpdate') + SessionAmplify.get('lastStockUpdate')) + (lastBoughtStockRefresh.time * (lastBoughtStockRefresh.count + 1) );
	ResetMyRandom(randSeed);

	if (!SessionAmplify.equals('page', 'shop')) {
		Session.set('newShopStock', true);
	}

	var items = [];

	var level = SessionAmplify.get('hero.level');

	var equipTypeIndex = Math.floor(MyRandom.fraction() * equipTypes.length);

	for (var i = 0; i<shopItemCount; i++) {
		var item = {};
		item.shopIndex = i;

		item.type = equipTypes[equipTypeIndex];
		equipTypeIndex++;
		equipTypeIndex = equipTypeIndex%equipTypes.length;

		item.nameSeed = randSeed + i;
		item.level = level;

		item.bonus = Math.floor(MyRandom.fraction() * 16) - 5;
		if (item.bonus < 0) {
			item.bonus = 0;
		}
		item.level += item.bonus;

		item.cost = Math.floor(Math.pow(item.level, 1.1) * Math.pow(item.level, 0.15));

		item.effects = createRandomEffects(item);

		items.push(item);
	}

	Session.set('shopItems', items);
}

var stockUpdateDelay = config.stockRefreshRate;
var updateStockIntervall = function () { // auto update shop
	var current = +new Date;
	var nextStockUpdate = SessionAmplify.get('nextStockUpdate');
	var lastStockUpdate = SessionAmplify.get('lastStockUpdate');

	nextStockUpdate -= current - lastStockUpdate;
	lastStockUpdate = current;

	if (nextStockUpdate <= 0) {
		nextStockUpdate = nextStockUpdate%stockUpdateDelay + stockUpdateDelay;

		SessionAmplify.set('nextStockUpdate', nextStockUpdate);
		SessionAmplify.set('lastStockUpdate', lastStockUpdate);

		var lastBoughtStockRefresh = SessionAmplify.get('lastBoughtStockRefresh');
		//lastBoughtStockRefresh.time = current;
		lastBoughtStockRefresh.count = 0;

		var base = config.stockRefreshBase;
		var baseCost = config.stockRefreshBaseCost;
		var level = SessionAmplify.get('hero.level');

		SessionAmplify.set('lastBoughtStockRefresh', lastBoughtStockRefresh);
		SessionAmplify.set('stockRefreshCost', getRefreshCost(baseCost, base, level, lastBoughtStockRefresh.count));

		updateStock();
	} else {
		SessionAmplify.set('nextStockUpdate', nextStockUpdate);
		SessionAmplify.set('lastStockUpdate', lastStockUpdate);
	}

	Meteor.setTimeout(updateStockIntervall, 1000);
}
var getRefreshCost = function (baseCost, base, level, count) {
	return baseCost * Math.pow(base, count) * level;
}

var buyStockRefresh = function () {
	var base = config.stockRefreshBase;
	var baseCost = config.stockRefreshBaseCost;

	var boughtUpdate = SessionAmplify.get('lastBoughtStockRefresh');
	var lastStockUpdate = SessionAmplify.get('lastStockUpdate');
	
	var cost = SessionAmplify.get('stockRefreshCost');

	var gold = SessionAmplify.get('hero.gold');
	SessionAmplify.set('hero.gold', gold - cost);

	boughtUpdate.count ++;
	boughtUpdate.time = +new Date;

	var level = SessionAmplify.get('hero.level');
	cost = getRefreshCost(baseCost, base, level, boughtUpdate.count);

	SessionAmplify.set('lastBoughtStockRefresh', boughtUpdate);
	SessionAmplify.set('stockRefreshCost', cost);

	updateStock();
}

updateStock();
updateStockIntervall();

Deps.autorun(function () {
	if (SessionAmplify.equals('page', 'shop')) {
		Session.set('newShopStock', false);
	}
});
/*Template.shop.canBuyRefresh = function () {
	var gold = SessionAmplify.get('hero.gold');
	var stockRefreshCost = SessionAmplify.get('stockRefreshCost');
	return gold >= stockRefreshCost;
}*/
Template.shop.buyReshreshDisabled = function () {
	var gold = SessionAmplify.get('hero.gold');
	var stockRefreshCost = SessionAmplify.get('stockRefreshCost');
	return gold < stockRefreshCost ? { disabled: true } : {};
}
Template.shop.stockRefreshCost = function () {
	return SessionAmplify.get('stockRefreshCost');
}

Template.shop.shopItems = function () {
	return Session.get('shopItems');
}

Template.shop.buyItemDisabled = function () {
	var inventar = SessionAmplify.get('inventar.'+this.type);
	var equiped = SessionAmplify.get('equipment.'+this.type);
	
	if (equiped && equiped.nameSeed === this.nameSeed) { // TODO nicht auf nameseed prüfen
		return {disabled: true};
	} 
	for (var i in inventar) {
		if (inventar[i].nameSeed === this.nameSeed) {
			return {disabled: true};
		}
	}

	return Session.equals('shop.sufficientGold.'+this.shopIndex, true) ? {} : {disabled: true};
}

Template.compareitem.equipped = function () {
	var type = Session.get('shop.comparetype');
	if (!type)
		return type;
	
	var equipped = SessionAmplify.get('equipment.' + type);
			
	return equipped;

}

Deps.autorun(function () {
	var gold = SessionAmplify.get('hero.gold');
	var shopItems = Session.get('shopItems');

	for (var i=0; i<shopItems.length; i++) {
		Session.set('shop.sufficientGold.'+i, gold>=shopItems[i].cost);
	}
});

Template.shop.events({
	'click #buyStockRefresh' : function () {
		buyStockRefresh();
	},
	'click .buyItem': function () {
		buyItem(this);
	},
	'mouseenter .shopItem' : function (e,t) {
		var item = e.target;
		var classes = item.getAttribute('class').split(' ');
		var type = classes[classes.indexOf('shopItem')+1];
		$(item).addClass('comparedItem');

		Session.set('shop.comparetype', type);
		//console.log($(item).offset());

		Meteor.setTimeout(function () {
			var pos = $(item).offset();
			$('#compareItem').css({
				top: (pos.top - 20) + 'px',
				left: (pos.left + 320) + 'px',
			}).show();
		}, 100);
	},
	'mouseleave .shopItem' : function (e,t) {
		//console.log('leave');
		var item = e.target;
		$(item).removeClass('comparedItem');
		Session.set('shop.comparetype', false);
	},
	'scroll #shopList' : function (e,t) {
		var comparetyp = Session.get('shop.comparetype');

		if (!(typeof comparetyp === 'undefined' || !comparetyp)) {
			var item = $('.comparedItem');
			var pos = item.offset();
			$('#compareItem').css({
				top: (pos.top - 20) + 'px',
				left: (pos.left + 320) + 'px',
			})
		}
	}
})