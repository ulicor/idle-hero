Deps.autorun(function () {
	var gold = SessionAmplify.get('hero.gold');
	if (!isFinite(gold) || gold > 1e+22) {
		SessionAmplify.set('hb', true);
		SessionAmplify.set('hb2', true);
		SessionAmplify.set('hero.gold', 0);
		SessionAmplify.set('hero.baseEffects', baseEffectsDefault);
	}
});