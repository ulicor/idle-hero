var defaultPlayer = {
	name: 'Hero',
	gender: 'm',
}
SessionAmplify.setDefault('playerSettings', defaultPlayer);

var updatePlayerSettings = function (data) {
	var success = [];

	var player = SessionAmplify.get('playerSettings');

	_.each(data, function (value, key, list) {
		if (player[key] !== value) {
			if (key === 'name') {
				player.nameChoosen = true;
			}
			player[key] = value;
			if (typeof value === 'boolean') {
				success.push('success_'+key+'_'+value);
			} else {
				success.push('success_'+key);
			}
		}
	});

	if (success.length > 0) {
		track({
			type: 'playerSettingsChanged',
			player : player,
		});
		SessionAmplify.set('playerSettings', player);
	}
	Session.set('settings.success', success); 
}

Template.settings.player = function () {
	return SessionAmplify.get('playerSettings');
}

Template.settings.genderIsChecked = function (compareTo) {
	var player = SessionAmplify.get('playerSettings');
	return player.gender === compareTo ? { checked: true } : {};
}

Template.settings.errors = function () {
	var nameE = Session.get('settings.name.errors');
	var nE = Session.get('settings.errors');
	var errors = nE.concat(nameE);

	if (errors.length == 0){
		Session.set('settings.hasError', false);
	}
	return errors;
}

Template.settings.rendered = function () {
	Session.set('settings.hasError', false);
	Session.set('settings.success', []); 
}


var lazySafeName = _.debounce(function (e) {
		var data = {};
		
		var errors = [];
		var hasError = Session.get('settings.hasError');

		data.name = e.target.value;
		Session.set('settings.success', []); 
		if (data.name === "") {
			hasError = true;
			errors.push("error_name_empty"); 
		} else {
			updatePlayerSettings(data);
		}

		Session.set('settings.hasError', hasError);
		Session.set('settings.name.errors', errors);
	}, 300
);

var lazySafeEmail = _.debounce(function (e) {
		var data = {};

		data.email = e.target.value;
		Session.set('settings.success', []); 
	
		updatePlayerSettings(data);
	}, 300
);

Template.settings.disableOfflineProgressChecked = function () {
	var player = SessionAmplify.get('playerSettings');
	return (typeof player.disableOfflineProgress !== "undefined" && player.disableOfflineProgress) ? {checked: true} : {};
}

Template.settings.newsletterChecked = function () {
	var player = SessionAmplify.get('playerSettings');
	return (typeof player.newsletter !== "undefined" && player.newsletter) ? {checked: true} : {};
}

Template.settings.events({
	'keyup #settings-name' : function (e, t) {
		lazySafeName(e);
	},
	'change [name="settings-gender"]' : function (e, t) {
		var data = {};
		var errors = [];
		var hasError = Session.get('settings.hasError');

		Session.set('settings.success', []); 
		data.gender = e.target.value;

		updatePlayerSettings(data);
	},
	'keyup #settings-email' : function (e, t) {
		lazySafeEmail(e);
	},
	'change #settings-newsletter' : function (e, t) {
		var data = {};

		newsletterInput = $(t.find('#settings-newsletter'));
		data.newsletter = newsletterInput.is(':checked');

		Session.set('settings.success', []); 

		updatePlayerSettings(data);
	},
	'change #settings-disableOfflineProgress' : function (e, t) {
		var data = {};

		disableOfflineProgressInput = $(t.find('#settings-disableOfflineProgress'));
		data.disableOfflineProgress = disableOfflineProgressInput.is(':checked');

		Session.set('settings.success', []); 

		updatePlayerSettings(data);
	},
	'click #settings-submit' : function (e, t) {
		var data = {};
		var errors = [];
		var hasError = false;

		Session.set('settings.success', []); 
		nameInput = $(t.find('#settings-name'));
		genderInput = $(t.find('[name="settings-gender"]:checked'));
		disableOfflineProgressInput = $(t.find('#settings-disableOfflineProgress'));

		data.name = nameInput.val();
		data.gender = genderInput.val();
		data.disableOfflineProgress = disableOfflineProgressInput.is(':checked');

		if (data.name === "") {
			hasError = true;
			errors.push("name_empty"); 
		}

		Session.set('settings.hasError', hasError);
		Session.set('settings.errors', errors); 

		// submit data
		if (!hasError) {
			updatePlayerSettings(data);
		}
	}
})