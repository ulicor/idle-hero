var heroPosition = {
	x: 7,
	y: 5,
}
var heroMoveSpeed = 150;
heroTile = null;

Session.setDefault('dungeonTiles', []);
Session.setDefault('dungeonKills', 0);

dungeonWidth = 14;
dungeonHeight = 12;

dungeonExploded = false;
unexplodeDungeon = function () {
	//if (dungeonExploded) {
	//	$( "#dungeon" ).stop().hide().effect('explode', {pieces: 9, mode: "show"}, 500, function () {
	//		dungeonExploded = false;
	//	} );
	//}
}
explodeDungeon = function (callback) {
	//$( "#dungeon" ).stop().effect('explode', {pieces: 9, mode: "hide"}, 500, callback);
	callback();
}

Template.dungeon.rendered = function () {
	unexplodeDungeon();
}

var dungeonTileTypes = {
	start: {
		probability: 0,
		walkable: true,
		onEnter: function () {
		}
	},
	empty: {
		probability: 100,
		walkable: true,
		onEnter: function () {
		}
	},
	monster: {
		probability: 30,
		walkable: true,
		onEnter: function () {
			var dungeonTiles = Session.get('dungeonTiles');
			var tile = dungeonTiles[heroPosition.y][heroPosition.x];

			tile.type = 'empty';
			Session.set('dungeonTiles', dungeonTiles);
			Session.set('dungeonKills', Session.get('dungeonKills') + 1);

			explodeDungeon(function () {
				dungeonExploded = true;
				initBattle();
				Session.set('monster.id', tile.monsterId);
				setNextAutoBattle('dungeonBattle');
				if (!Session.equals('combatmode', 'dungeon'))
					Session.set('combatmode', 'dungeon');
			});
		},
		init: function (tile) {
			tile.monsterId = Random.choice(possibleMonsters);
			tile.additionalCss = 'background-image: url("/monsters/'+tile.monsterId+'.png");';
		}
	},
	finish: {
		probability: 0,
		walkable: true,
		onEnter: function () {
			var level = SessionAmplify.get('hero.level');
			var exp = SessionAmplify.get('hero.exp');
			var gold = SessionAmplify.get('hero.gold');
			var dungeonKills = Session.get('dungeonKills');

			var selectedMonsterLevel = SessionAmplify.get('selectedMonsterLevel');

			var expGain = dungeonKills * 40 * calcExpModifierBasedOnLevel(selectedMonsterLevel, level);
			var goldGain = dungeonKills * 2 * Math.pow(selectedMonsterLevel, 1.1);

			exp += expGain;
			gold += goldGain;

			var levelUps = 0;
			if (exp >= 100) {

				levelUps = Math.floor(exp/100);

				SessionAmplify.set('hero.level', level+levelUps);
				showLevelUpMessage();

				exp %= 100;
			}

			SessionAmplify.set('hero.exp', exp);

			SessionAmplify.set('hero.gold', gold);

			SessionAmplify.set('dungeonsCompleted', SessionAmplify.get('dungeonsCompleted')+1);

			showMessage('notice', 'dungeonReward', {data:{levelUps: levelUps, goldGain: goldGain}, timeout: 5000});

			track({
				type: 'receiveDungeonReward',
				level: level,
				levelUps: levelUps,
				selectedMonsterLevel: selectedMonsterLevel,
				gold: gold,
				expGain: expGain,
				goldGain: goldGain,
			});

			SessionAmplify.set('selectedMonsterLevel', selectedMonsterLevel+1);

			initDungeon();
			explodeDungeon(function () {
				dungeonExploded = true;
				unexplodeDungeon();
			})
		}
	},
	wall: {
		probability: 100,
		walkable: false,
		onEnter: function () {
		}
	},
}

var pushWall = function (walls, dungeonTiles, wall) {
	if (!dungeonTiles[wall.y][wall.x].walkable) {
		dungeonTiles[wall.y][wall.x].entrances++;
		walls.push(wall);
	}
}

var addNeighourWalls = function (walls, dungeonTiles, x, y) {
	if (x < dungeonWidth-1) {
		pushWall(walls, dungeonTiles, {
			x: x+1,
			y: y,
		});
	}
	if (x > 0) {
		pushWall(walls, dungeonTiles, {
			x: x-1,
			y: y,
		});
	}
	if (y < dungeonHeight-1) {
		pushWall(walls, dungeonTiles, {
			x: x,
			y: y+1,
		});
	}
	if (y > 0) {
		pushWall(walls, dungeonTiles, {
			x: x,
			y: y-1,
		});
	}
}

var selectTypeFromRange = function (sum, range) {
	var rand = Math.floor(Math.random()*sum);
	for (var i in range) {
		if (rand >= range[i].min && rand < range[i].max) {
			return range[i].type;
		}
	}
}

initDungeon = function () {
	Session.set('monster.id', null);
	var dungeonTiles = [];
	//showMessage('notice', 'newdungeon', {timeout: 10000, data: {text: 'newdungeon', level:SessionAmplify.get('selectedMonsterLevel')}});
	for (var y = 0; y<dungeonHeight; y++) {
		dungeonTiles.push([]);
		for (var x = 0; x<dungeonWidth; x++) {
			dungeonTiles[y][x] = {
				x: x,
				y: y,
				walkable: false,
				type: '',
				entrances: 0,
				additionalCss: '',
			}
		}
	}

	var walls = [];
	var finishTile = 

	dungeonTiles[heroPosition.y][heroPosition.x].walkable = true;
	dungeonTiles[heroPosition.y][heroPosition.x].type = 'start';
	addNeighourWalls(walls, dungeonTiles, heroPosition.x, heroPosition.y);

	while(walls.length > 0) {
		var wall = walls.splice(Math.floor(Math.random()*walls.length), 1)[0];

		if (dungeonTiles[wall.y][wall.x].entrances === 1 && !dungeonTiles[wall.y][wall.x].walkable) {
			dungeonTiles[wall.y][wall.x].walkable = true;
			addNeighourWalls(walls, dungeonTiles, wall.x, wall.y);
			finishTile = wall;
		}
	}

	dungeonTiles[finishTile.y][finishTile.x].type = 'finish';
	

	//define cell types
	var walkableProbSum = 0;
	var walkableProbRanges = [];
	var unwalkableProbSum = 0;
	var unwalkableProbRanges = [];
	for (var i in dungeonTileTypes) {
		if (dungeonTileTypes[i].probability > 0) {
			if (dungeonTileTypes[i].walkable) {
				var newSum = walkableProbSum+dungeonTileTypes[i].probability;
				walkableProbRanges.push({
					min: walkableProbSum,
					max: walkableProbSum+dungeonTileTypes[i].probability,
					type: i,
				});
				walkableProbSum = newSum;
			} else {
				var newSum = unwalkableProbSum+dungeonTileTypes[i].probability;
				unwalkableProbRanges.push({
					min: unwalkableProbSum,
					max: unwalkableProbSum+dungeonTileTypes[i].probability,
					type: i,
				});
				unwalkableProbSum = newSum;
			}
		}
	}

	for (var y = 0; y<dungeonHeight; y++) {
		for (var x = 0; x<dungeonWidth; x++) {
			delete dungeonTiles[y][x].entrances;
			if (dungeonTiles[y][x].type === '') {
				if (dungeonTiles[y][x].walkable) {
					dungeonTiles[y][x].type = selectTypeFromRange(walkableProbSum, walkableProbRanges);
				} else {
					dungeonTiles[y][x].type = selectTypeFromRange(unwalkableProbSum, unwalkableProbRanges);
				}

				if (typeof dungeonTileTypes[dungeonTiles[y][x].type].init === 'function') {
					dungeonTileTypes[dungeonTiles[y][x].type].init(dungeonTiles[y][x]);
				} 
			}
		}
	}

	Session.set('dungeonTiles', dungeonTiles);
	Session.set('dungeonKills', 0);

	updateHeroPosition(false);
}

lazyInitDungeon = _.debounce(function () {
	initDungeon();
	explodeDungeon(function () {
		dungeonExploded = true;
		unexplodeDungeon();
	});
}, 300);

var updateHeroPosition = function (animate) {
	if (!heroTile || heroTile.length === 0) {
		return Meteor.setTimeout(function () {updateHeroPosition(animate)}, 50);
	}

	var target = {
		left: heroPosition.x*32,
		top: heroPosition.y*32,
	};

	if (animate) {
		heroTile.stop().animate(target, heroMoveSpeed, onHeroEnterTile);
	} else {
		heroTile.css(target);
	}
}

var onHeroEnterTile = function () {
	var dungeonTiles = Session.get('dungeonTiles');
	dungeonTileTypes[dungeonTiles[heroPosition.y][heroPosition.x].type].onEnter();
}

var tryMoveHero = function (newHeroPosition) {
	if (heroTile.is(':animated')) {
		return false;
	}
	if (newHeroPosition.x < 0 || newHeroPosition.y < 0 || newHeroPosition.x >= dungeonWidth || newHeroPosition.y >= dungeonHeight) {
		return false;
	}
	var dungeonTiles = Session.get('dungeonTiles');
	if (!dungeonTiles[newHeroPosition.y][newHeroPosition.x].walkable) {
		return false;
	}

	setNextAutoBattle('moveDungeon');
	if (!Session.equals('combatmode', 'dungeon'))
		Session.set('combatmode', 'dungeon');

	heroPosition = newHeroPosition;
	updateHeroPosition(true);
	return true;
}

initDungeon();

Template.heroTile.rendered = function () {
	heroTile = $('#heroTile');
	updateHeroPosition(false);
}

Template.heroTile.playerImg = function (gender) {
	if (SessionAmplify.get('playerSettings').gender==='m') {
		return '/heroes/hero.png';
	}
	return '/heroes/heroin.png';
}

$(document).bind('keydown', function(e) {
	if (!heroTile || heroTile.length === 0 || !heroTile.is(':visible')) {
		return;
	}

	switch (e.which) {
		case 87:
		case 38:
			//up
			e.preventDefault();
			tryMoveHero({
				x: heroPosition.x,
				y: heroPosition.y-1,
			});
			break;
		case 65:
		case 37:
			//left
			e.preventDefault();
			tryMoveHero({
				x: heroPosition.x-1,
				y: heroPosition.y,
			});
			break;
		case 83:
		case 40:
			//down
			e.preventDefault();
			tryMoveHero({
				x: heroPosition.x,
				y: heroPosition.y+1,
			});
			break;
		case 68:
		case 39:
			//right
			e.preventDefault();
			tryMoveHero({
				x: heroPosition.x+1,
				y: heroPosition.y,
			});
			break;
	}
});