idle-hero
-=========-

---Features---
-Fight different levels of monsters
-Buy new Equipment in the Shop which regularly receives a new randomly generated stock of items
-Manage different sets of equipment in your equipment with your inventory
-Take quests in the Tavern and earn extra rewards



---Todos---
-Visualize Progress Tracking
-combine inventory and shop
-Make Quest durations scalable with a bar
-Achievements - Tutorial - Notifier
-Dungeons
-Bosse
-Language - alles ausser english popup
-rework items, they should be more depending on level and less on bonus
-pets
-let brawling drop keys which you require to enter a dungeon(?)
-extra reward for killing all monsters in a dungeon(?)

---Tutorial/FAQ---
-zu schneller anfang, am anfang weiß man nicht was passiert und steigt schon level bevor man versteht was man tun soll
-trigger tutorial if expGain is way to low
-trigger tutorial if you do not click at all
-trigger tutorial if you dont have any items
-trigger tutorial if you never started a quest


---Backlog---
-wiki?
-musik?
-hp wichtiger machen
-stats screen, how many clciks, how many monsters etc


---latest changelog----
v0.1.0
-increased gold gain per HP at higher monster levels, overall increasing gold gain
-slightly decreased shop prices at the beginning but increased them at higher levels
-increased starting gold to 100
-fixed a bug in the tavern which crashed your game when buying a non number amount of skills

v0.1.2
-increased crit chance and crit damage
-increased the amount of bonus attack on weapons and bonus hp on armor
-added hp as a guaranteed effect on accessories
-attack speed does no longer increase the speed of the automatic battle but rather increases the amount of attacks done at once, this also prevents the monster from hitting back faster
-attack speed is now calculated into dps
-buffed monster hp and damage
-removed language auto detection
-reworked crit, crit dmg and attack speed item ratings to not scale down on level up
-nerfed high level gold gain items and buffed low level ones
-inside shop made the comparing stats to the current equip more noticeable
-made the welcome back message box more nice
-added a setting to disable offline progress
-added some flavor texts to the tavern
-added an indicator to the tavern when a quest reward is ready to be collected
-fixed a bug which prevented score from updating
-in shop on mouse over displaying the currently equipped item
-displaying some more stats (like hit chance) below the monster

sneakPatch (no new version)
-fixed a bug which caused quest gold reward to not scale with level
-added more quests
-increased the chance for the shop to create higher quality items by roughly 25%

sneakPatch2 (no new version)
-added a toelrance for the exp penalty of 10% of your level or atmost 1000, to make idling for longer period of time more worthwhile

v0.1.5
-implemented dungeons
-doubled auto attack speed
-FAQ removed
-added a info message when you die
-improved loading speed of the game
-fixed a bug which caused the option to disable offline progress to not work as intended
-fixed a bug which caused the item comparison in shop to disappear
-fixed a bug which caused the hpBars to sometimes animate into the wrong direction

v0.1.7
-combined monsters and dungeons to combat. Possibility to choose between brawling and dungeon.
-set auto attack speed back to once per second
-increased dungeon reward
-added more message notification.
-updated some i18n de_DE texts.
-removed the dungeon/battle transition animation due to performance issues
-new hiscores ehp, dps, monsters killed and dungeons completed

sneakPatch
-when finishing a dungeon increasing enemy level by 1
-on death reducing enemy level by 1
-surpressed death message caused by offline progress
-fixed a bug which caused offline progress to not be calcualted completely

sneakPatch2
-fixed a bug which prevented tavern prices from resetting
-changed dungeon reward to be dependant on the amount of monsters killed in that dungeon
-changed menu order to have tavern at the end


---Deployment---
-https://atmosphere.meteor.com/package/GAnalytics
-http://docs.meteor.com/#meteor_settings
-meteor deploy idle-hero --settings settings.json
-meteor deploy idle-hero-staging --settings settings.json
-password = ksn + MRT / nobody0